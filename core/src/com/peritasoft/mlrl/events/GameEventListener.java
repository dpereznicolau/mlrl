/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.events;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.item.Potion;

public interface GameEventListener {
    void attackHit(Character attacker, Character victim, int damage, ItemCategory itemCategory);

    void attackMissed(Character attacker, Character victim);

    void died(PlayerHero player);

    void drank(Potion potion, Character drinker);

    void pickedUp(Character character, Item item);

    void killed(Character c);

    void lookedAt(Item item);

    void luredByDecoy(Character enemy);

    void openedChest(Item item);

    void spottedPlayer(Character enemy);

    void fireSpellgemCracked();

    void levelUp(int newLevel, int strUp, int dexUp, int wisUp, int conUp);

    void teleported();

    void teleported(Character blinker);

    void healed(Character character, int hpHealed);

    void castFireNova();

    void castIceNova();

    void petrified(Character character, int turns);

    void confused(Character character);

    void skipPetrifiedTurn(Character character, int turns);

    void skipFrozenTurn(Character character, int turns);

    void notEnoughWis();

    void notEnoughMana(Character character);

    void attackByPoison(Character attacker, Character victim, int damage);

    void shootMissed();

    void openedEmptyChest();

    void raiseStat(Character character, String stat, int amountRaised);

    void changedFloor(int floor, Level level);

    void inventoryFull();

    void weaponEmbedded(Character attacker);

    void newGame(long seed);

    void pauseGame();

    void resumeGame(long seed);

    void summonMob(Character summoner, Character summoned);

    void raised(Character character);

    void intangibleTarget(Character character);

    void canNotShootSelf(Character character);

    void inspectBookshelf(Item item);

    void equipWeapon(Character character);

    void playerMoves();

    void openInventory();

    void closeInventory();

    void poison();

    void castLightningStorm();

    void dispelledEffects(Character character);

    void foundCoins();

    void attackByFire(Character attacker, Character victim, int damage);

    void castFireWall();

    void endGame(PlayerHero player);

    void deathChangedPersona();
}
