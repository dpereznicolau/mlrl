/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.events;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.item.Potion;

public class SfxManager extends GameEventAdapter implements Disposable {
    private boolean enabled = true;
    private final Sound impact;
    private final Sound miss;
    private final Sound levelUp;
    private final Sound crack;
    private final Sound step;
    private final Sound chest_open;
    private final Sound chest_locked;
    private final Sound drink;
    private final Sound open_inventory;
    private final Sound close_inventory;
    private final Sound summon;
    private final Sound firenova;
    private final Sound icenova;
    private final Sound lightningStorm;
    private final Sound confused;
    private final Sound paralyzed;
    private final Sound poison;
    private final Sound teleport;
    private final Sound taunt;
    private final Sound sewers_mob_dies;
    private final Sound human_dies;
    private final Sound minotaur_dies;
    private final Sound goblin_dies;


    public SfxManager() {
        impact = Gdx.audio.newSound(Gdx.files.internal("sfx/impact_a.wav"));
        miss = Gdx.audio.newSound(Gdx.files.internal("sfx/miss.wav"));
        levelUp = Gdx.audio.newSound(Gdx.files.internal("sfx/level_up.wav"));
        crack = Gdx.audio.newSound(Gdx.files.internal("sfx/cracking_a.wav"));
        step = Gdx.audio.newSound(Gdx.files.internal("sfx/step.wav"));
        chest_open = Gdx.audio.newSound(Gdx.files.internal("sfx/Chest_Open.wav"));
        chest_locked = Gdx.audio.newSound(Gdx.files.internal("sfx/Chest_Locked.wav"));
        open_inventory = Gdx.audio.newSound(Gdx.files.internal("sfx/TravelBag_Examine_Open.wav"));
        close_inventory = Gdx.audio.newSound(Gdx.files.internal("sfx/TravelBag_Examine_Close.wav"));
        drink = Gdx.audio.newSound(Gdx.files.internal("sfx/Drink_02.wav"));
        summon = Gdx.audio.newSound(Gdx.files.internal("sfx/summon.wav"));
        firenova = Gdx.audio.newSound(Gdx.files.internal("sfx/firenova.wav"));
        icenova = Gdx.audio.newSound(Gdx.files.internal("sfx/ice_crack.wav"));
        lightningStorm = Gdx.audio.newSound(Gdx.files.internal("sfx/lightningStorm.wav"));
        confused = Gdx.audio.newSound(Gdx.files.internal("sfx/confuse.wav"));
        paralyzed = Gdx.audio.newSound(Gdx.files.internal("sfx/paralyzed.wav"));
        poison = Gdx.audio.newSound(Gdx.files.internal("sfx/poison.wav"));
        teleport = Gdx.audio.newSound(Gdx.files.internal("sfx/teleport.wav"));
        sewers_mob_dies = Gdx.audio.newSound(Gdx.files.internal("sfx/sewers_mob_dies.wav"));
        taunt = Gdx.audio.newSound(Gdx.files.internal("sfx/taunt.wav"));
        human_dies = Gdx.audio.newSound(Gdx.files.internal("sfx/human.wav"));
        minotaur_dies = Gdx.audio.newSound(Gdx.files.internal("sfx/minotaur.wav"));
        goblin_dies = Gdx.audio.newSound(Gdx.files.internal("sfx/goblin.wav"));
        GameEvent.register(this);
    }

    @Override
    public void attackHit(Character attacker, Character victim, int damage, ItemCategory itemCategory) {
        switch (itemCategory) {
            case SCROLL_FIRE:
            case SCROLL_ICE:
            case SCROLL_POISON:
                //area effects don't play impact sound
                break;
            default:
                if (enabled) impact.play();
        }

    }

    @Override
    public void attackMissed(Character attacker, Character victim) {
        if (enabled) miss.play();
    }

    @Override
    public void poison() {
        if (enabled) poison.play();
    }

    @Override
    public void attackByPoison(Character attacker, Character victim, int damage) {
        if (enabled) poison.play();
    }

    @Override
    public void fireSpellgemCracked() {
        if (enabled) crack.play();
    }

    @Override
    public void levelUp(int newLevel, int strUp, int dexUp, int wisUp, int conUp) {
        if (enabled) levelUp.play();
    }

    @Override
    public void playerMoves() {
        if (enabled) step.play();
    }

    @Override
    public void openedChest(Item item) {
        if (enabled) chest_open.play();
    }

    @Override
    public void openedEmptyChest() {
        if (enabled) chest_locked.play();
    }

    @Override
    public void drank(Potion potion, Character drinker) {
        if (enabled) drink.play();
    }

    @Override
    public void summonMob(Character summoner, Character summoned) {
        if (enabled) summon.play();
    }

    @Override
    public void openInventory() {
        if (enabled) open_inventory.play();
    }

    @Override
    public void closeInventory() {
        if (enabled) close_inventory.play();
    }

    @Override
    public void castFireNova() {
        if (enabled) firenova.play();
    }

    @Override
    public void castFireWall() {
        if (enabled) firenova.play();
    }

    @Override
    public void castIceNova() {
        if (enabled) icenova.play();
    }

    @Override
    public void castLightningStorm() {
        if (enabled) lightningStorm.play();
    }

    @Override
    public void confused(Character character) {
        if (enabled) confused.play();
    }

    @Override
    public void petrified(Character character, int turns) {
        if (enabled) paralyzed.play();
    }

    @Override
    public void teleported() {
        if (enabled) teleport.play();
    }

    @Override
    public void killed(Character c) {
        if (enabled) {
            switch (c.getDemography().race) {
                case BEETLE:
                case BAT:
                case RAT:
                case SPIDER:
                case SNAKE:
                case SLIME:
                case JELLY:
                    sewers_mob_dies.play();
                    break;
                case MINOTAUR:
                    minotaur_dies.play();
                    break;
                case GOBLIN:
                    goblin_dies.play();
                    break;
                case BOSS:
                case HUMAN:
                    human_dies.play();
                    break;
            }
        }
    }

    @Override
    public void spottedPlayer(Character enemy) {
        if (enabled) taunt.play();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void toggle() {
        enabled = !enabled;
    }

    @Override
    public void dispose() {
        GameEvent.unregister(this);
        impact.dispose();
        miss.dispose();
        levelUp.dispose();
        crack.dispose();
        step.dispose();
        chest_open.dispose();
        chest_locked.dispose();
        open_inventory.dispose();
        close_inventory.dispose();
        drink.dispose();
        summon.dispose();
        firenova.dispose();
        icenova.dispose();
        lightningStorm.dispose();
        confused.dispose();
        paralyzed.dispose();
        poison.dispose();
        teleport.dispose();
        sewers_mob_dies.dispose();
        taunt.dispose();
        human_dies.dispose();
        minotaur_dies.dispose();
        goblin_dies.dispose();
    }

}
