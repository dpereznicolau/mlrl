/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.events;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.item.Potion;

public class GameEventAdapter implements GameEventListener {

    @Override
    public void attackHit(Character attacker, Character victim, int damage, ItemCategory itemCategory) {
    }

    @Override
    public void attackMissed(Character attacker, Character victim) {
    }

    @Override
    public void died(PlayerHero player) {
    }

    @Override
    public void drank(Potion potion, Character drinker) {
    }

    @Override
    public void pickedUp(Character character, Item item) {
    }

    @Override
    public void killed(Character c) {
    }

    @Override
    public void lookedAt(Item item) {
    }

    @Override
    public void luredByDecoy(Character enemy) {
    }


    @Override
    public void openedChest(Item item) {
    }

    @Override
    public void spottedPlayer(Character enemy) {
    }

    @Override
    public void fireSpellgemCracked() {
    }

    @Override
    public void levelUp(int newLevel, int strUp, int dexUp, int wisUp, int conUp) {
    }

    @Override
    public void teleported() {
    }

    @Override
    public void teleported(Character blinker) {
    }

    @Override
    public void healed(Character character, int hpHealed) {

    }

    @Override
    public void castFireNova() {

    }

    @Override
    public void castIceNova() {

    }

    @Override
    public void petrified(Character character, int turns) {

    }

    @Override
    public void confused(Character character) {
    }

    @Override
    public void skipPetrifiedTurn(Character character, int turns) {

    }

    @Override
    public void skipFrozenTurn(Character character, int turns) {

    }

    @Override
    public void notEnoughWis() {

    }

    @Override
    public void notEnoughMana(Character character) {

    }

    @Override
    public void attackByPoison(Character attacker, Character victim, int damage) {

    }

    @Override
    public void shootMissed() {

    }

    @Override
    public void openedEmptyChest() {

    }

    @Override
    public void raiseStat(Character character, String stat, int amountRaised) {

    }

    @Override
    public void changedFloor(int floor, Level level) {
    }

    @Override
    public void inventoryFull() {

    }

    @Override
    public void weaponEmbedded(Character attacker) {

    }

    @Override
    public void newGame(long seed) {


    }

    @Override
    public void pauseGame() {

    }

    @Override
    public void resumeGame(long seed) {

    }

    @Override
    public void summonMob(Character summoner, Character summoned) {

    }

    @Override
    public void raised(Character character) {

    }

    @Override
    public void intangibleTarget(Character character) {

    }

    @Override
    public void canNotShootSelf(Character character) {

    }

    @Override
    public void inspectBookshelf(Item item) {

    }

    @Override
    public void equipWeapon(Character character) {

    }

    @Override
    public void playerMoves() {

    }

    @Override
    public void openInventory() {

    }

    @Override
    public void closeInventory() {

    }

    @Override
    public void poison() {

    }

    @Override
    public void castLightningStorm() {

    }

    @Override
    public void dispelledEffects(Character character) {

    }

    @Override
    public void foundCoins() {

    }

    @Override
    public void attackByFire(Character attacker, Character victim, int damage) {

    }

    @Override
    public void castFireWall() {

    }

    @Override
    public void endGame(PlayerHero player) {

    }

    @Override
    public void deathChangedPersona() {
    }
}
