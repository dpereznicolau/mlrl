/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.events;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.LevelType;

import java.util.HashMap;
import java.util.Map;

public class MusicManager extends GameEventAdapter implements Disposable {
    private Map<LevelType, Music> musics;
    private Music current;
    private boolean playing = true;

    public MusicManager() {
        musics = new HashMap<>();
        musics.put(LevelType.SEWER, Gdx.audio.newMusic(Gdx.files.internal("music/sewers.mp3")));
        musics.put(LevelType.DUNGEON, Gdx.audio.newMusic(Gdx.files.internal("music/dungeon.mp3")));
        musics.put(LevelType.CRYPT, Gdx.audio.newMusic(Gdx.files.internal("music/dungeon.mp3")));
        musics.put(LevelType.CAVE, Gdx.audio.newMusic(Gdx.files.internal("music/caves.mp3")));
        musics.put(LevelType.DEATH, Gdx.audio.newMusic(Gdx.files.internal("music/end.mp3")));
        GameEvent.register(this);
    }

    @Override
    public void changedFloor(int floor, Level level) {
        Music music = musics.get(level.getType());
        if (music == current) {
            return;
        }
        switchMusic(music);
    }

    private void switchMusic(Music music) {
        if (current != null) {
            current.stop();
        }
        current = music;
        if (current != null) {
            current.setLooping(true);
            current.setVolume(0.35f);
            play();
        }
    }

    @Override
    public void dispose() {
        GameEvent.unregister(this);
        switchMusic(null);
        for (Music m : musics.values()) {
            m.dispose();
        }
    }

    public void pause() {
        if (current != null) {
            current.pause();
        }
    }

    public void play() {
        if (current != null && playing) {
            current.play();
        }
    }

    public boolean isPlaying() {
        return playing;
    }

    public void toggle() {
        if (playing) {
            pause();
            playing = false;
        } else {
            playing = true;
            play();
        }
    }
}
