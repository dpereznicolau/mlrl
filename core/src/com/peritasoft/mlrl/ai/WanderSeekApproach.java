/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;

public class WanderSeekApproach extends Behaviour {
    private final boolean tauntPlayer;
    private boolean tauntedPlayer = false;
    private final ApproachAndAttack approachAndAttack;
    private final Behaviour idle;

    public WanderSeekApproach() {
        this(true);
    }

    public WanderSeekApproach(boolean tauntPlayer) {
        this(tauntPlayer, new ApproachAndAttack());
    }

    public WanderSeekApproach(boolean tauntPlayer, ApproachAndAttack approachAndAttack) {
        this.tauntPlayer = tauntPlayer;
        this.approachAndAttack = approachAndAttack;
        idle = new Wander();
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        boolean seenPlayer = false;
        Action action;
        if (enemy != null) {
            seenPlayer = true;
            action = approachAndAttack.update(level, me, enemy);
        } else {
            action = approachAndAttack.followLastKnownPath(level, me);
            if (action == Action.NONE) {
                action = idle.update(level, me, enemy);
            }
        }

        if (seenPlayer && tauntPlayer) {
            if (!tauntedPlayer) {
                tauntedPlayer = true;
                GameEvent.spottedPlayer(me);
            }
        } else {
            tauntedPlayer = false;
        }
        return action;
    }
}
