/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.Crosshairs;

public class StayAtRangeAndShoot extends Behaviour {
    private final Behaviour approach;
    private final Behaviour runaway;
    private final Behaviour noArrowsBehaviour;
    private final int idleTurns;
    private int idleTurnCount;

    public StayAtRangeAndShoot(Behaviour noArrowsBehaviour, int idleTurns) {
        approach = new WanderSeekApproach();
        runaway = new Runaway();
        this.noArrowsBehaviour = noArrowsBehaviour;
        this.idleTurns = idleTurns;
        this.idleTurnCount = 0;
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        idleTurnCount += 1;

        if (enemy == null) {
            return approach.update(level, me, enemy);
        }

        Position enemyPos = enemy.getPrevPosition();
        int distance = me.getPosition().distance(enemyPos);
        if (distance < 2) {
            return runaway.update(level, me, enemy);
        }

        if (idleTurnCount < idleTurns) {
            return Action.NONE;
        }
        idleTurnCount = 0;
        if (!me.canFireWeapon()) {
            return noArrowsBehaviour.update(level, me, enemy);
        }
        if (me.getFireRange() < distance) {
            return approach.update(level, me, enemy);
        }
        if (level.hasNoObstacles(me.getPosition(), enemyPos)) {
            Crosshairs xh = new Crosshairs(enemyPos, Direction.NONE, me.shootWeapon());
            me.shoot(level, xh);
            return Action.ATTACK;
        }
        return approach.update(level, me, enemy);
    }
}
