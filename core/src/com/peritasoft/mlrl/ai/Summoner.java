/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.*;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.SummonPortal;
import com.peritasoft.mlrl.events.GameEvent;

public class Summoner extends Behaviour {
    private final Behaviour idleBehaviour;
    private final int idleTurns;
    private final int maxSummonedChars;
    private int idleTurnCount;
    private int summonedCharsCount;
    private Demography summonedChar;
    private int summonedCharLevel;

    public Summoner(Behaviour idleBehaviour, int idleTurns, int maxSummonedChars, Demography summonedChar, int summonedCharLevel) {
        this.idleBehaviour = idleBehaviour;
        this.idleTurns = idleTurns;
        this.maxSummonedChars = maxSummonedChars;
        this.idleTurnCount = 0;
        this.summonedCharsCount = 0;
        this.summonedChar = summonedChar;
        this.summonedCharLevel = summonedCharLevel;
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        if (canSummon()) {
            idleTurnCount += 1;
            if (idleTurnCount == idleTurns) {
                idleTurnCount = 0;
                final Direction direction = enemy == null
                        ? me.getLastDirection()
                        : me.getPosition().directionTo(enemy.getPosition());
                if (summonChar(level, me, direction)) {
                    summonedCharsCount++;
                    return Action.SUMMON;
                } else {
                    idleTurnCount -= 2;
                }
            }
        }
        return idleBehaviour.update(level, me, enemy);
    }

    private boolean summonChar(Level level, Character me, Direction direction) {
        Position pos = new Position(me.getPositionX() + direction.x, me.getPositionY() + direction.y);
        if (level.isWalkable(pos) && !level.hasCharacterIn(pos)) {
            Character r;
            switch (summonedChar) {
                case SKELETON:
                    r = new Skeleton(summonedCharLevel, pos, false);
                    break;
                case RAT_GIANT:
                default:
                    r = new Rat(summonedCharLevel, pos, false);
                    break;
            }
            level.getLifeObjs().add(new SummonPortal(level, r));
            me.setLastDirection(direction);
            GameEvent.summonMob(me, r);
            return true;
        }
        return false;
    }

    private boolean canSummon() {
        return summonedCharsCount < maxSummonedChars;
    }
}
