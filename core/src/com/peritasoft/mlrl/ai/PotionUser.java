/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.Potion;
import com.peritasoft.mlrl.item.PotionType;

public class PotionUser extends Behaviour {
    Behaviour defaultBehaviour;

    public PotionUser(Behaviour defaultBehaviour) {
        this.defaultBehaviour = defaultBehaviour;
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        if (me.getHp() <= me.getMaxHp() / 3) {
            int potionIdx = hasHealthPotion(me);
            if (potionIdx > -1) {
                Item usable = me.getInventory().get(potionIdx);
                if (me.use(usable, level)) {
                    me.getInventory().remove(potionIdx);
                    return Action.USE_ITEM;
                }
            }
        }
        return defaultBehaviour.update(level, me, enemy);
    }

    private int hasHealthPotion(Character c) {
        for (int j = 0; j < c.getInventory().size(); j++) {
            Item i = c.getInventory().get(j);
            if (i instanceof Potion) {
                Potion p = (Potion) i;
                if (p.getType() == PotionType.HEALTH) {
                    return j;
                }
            }
        }
        return -1;
    }
}
