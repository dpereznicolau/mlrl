/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.*;

import java.util.Queue;

public class ApproachAndAttack extends Behaviour {
    private Queue<Position> path;
    private final Position pathGoal = new Position(-1, -1);
    private final boolean incorporeal;

    public ApproachAndAttack() {
        this(false);
    }

    public ApproachAndAttack(boolean incorporeal) {
        this.incorporeal = incorporeal;
    }

    @Override
    public Action update(final Level level, final Character me, final Character enemy) {
        if (enemy == null) {
            return me.move(Direction.atRandom(), level);
        }
        Position enemyPos = enemy.getPrevPosition();
        if (path == null || path.isEmpty() || !enemyPos.equals(pathGoal)) {
            pathGoal.set(enemyPos);
            path = PathFinder.find(level, me.getPosition(), pathGoal, new PathFinder.Traversor() {
                @Override
                public boolean isTraverseable(int x, int y) {
                    // Do not check if there is anyone at goal because somebody
                    // else character could have moved to it before us; if the
                    // player could be there, so can we.
                    if (pathGoal.equals(x, y)) {
                        return true;
                    }
                    Cell cell = level.getCell(x, y);
                    if ((!incorporeal && !cell.isWalkable()) || cell.tile == Tile.BOUND) {
                        return false;
                    }
                    Character character = cell.getCharacter();
                    return character == null || character == enemy;
                }
            });
            if (path == null || path.isEmpty()) {
                // Reaching this point means that we could not find a path
                // when taking other Characters in account, for instance
                // passing through a corridor, therefore we try again this
                // time ignoring other Characters; it is better that having
                // nothing to do.
                path = PathFinder.find(level, me.getPosition(), pathGoal, new PathFinder.Traversor() {
                    @Override
                    public boolean isTraverseable(int x, int y) {
                        final Cell cell = level.getCell(x, y);
                        return (incorporeal || cell.isWalkable()) && cell.tile != Tile.BOUND;
                    }
                });
                if (path == null || path.isEmpty()) {
                    // I give up.
                    return me.move(Direction.atRandom(), level);
                }
            }
        }
        return followLastKnownPath(level, me);
    }

    public Action followLastKnownPath(final Level level, final Character me) {
        if (path == null || path.isEmpty()) {
            return Action.NONE;
        }
        Position pos = path.remove();
        Direction dir = me.getPosition().directionTo(pos);
        Action action = me.move(dir, level);
        if (action == Action.COLLIDE) {
            path.clear();
        }
        return action;
    }
}
