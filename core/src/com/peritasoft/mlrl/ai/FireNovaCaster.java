/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.weapons.FireNova;

public class FireNovaCaster extends Behaviour {
    private final Behaviour idleBehaviour;
    private final int idleTurns;
    private int idleTurnCount;
    private final FireNova nova;

    public FireNovaCaster(Behaviour idleBehaviour, int idleTurns, int novaRadius, int novaDamage) {
        this.idleBehaviour = idleBehaviour;
        this.idleTurns = idleTurns;
        this.idleTurnCount = 0;
        nova = new FireNova(novaRadius, novaDamage, ItemCategory.GEM_RUBY);
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        idleTurnCount += 1;
        if (idleTurnCount >= idleTurns && enemy != null) {
            idleTurnCount = 0;
            nova.cast(me, level, me.getPosition());
            return Action.NONE;
        }
        return idleBehaviour.update(level, me, enemy);
    }

}
