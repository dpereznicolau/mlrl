/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.weapons.FireWall;

public class FireWallCaster extends Behaviour {
    private final Behaviour idleBehaviour;
    private final int idleTurns;
    private int idleTurnCount;
    private final FireWall fireWall;

    public FireWallCaster(Behaviour idleBehaviour, int idleTurns, int wallLength, int damage) {
        this.idleBehaviour = idleBehaviour;
        this.idleTurns = idleTurns;
        this.idleTurnCount = 0;
        fireWall = new FireWall(wallLength, MathUtils.random(3, 5), damage);
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        idleTurnCount += 1;
        if (idleTurnCount >= idleTurns && enemy != null) {
            idleTurnCount = 0;
            Direction dir = me.getPosition().directionTo(enemy.getPosition());
            Position initial = new Position(me.getPositionX() + dir.x, me.getPositionY() + dir.y);
            fireWall.cast(me, level, initial, dir);
            return Action.NONE;
        }
        return idleBehaviour.update(level, me, enemy);
    }

}
