/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.scores;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.peritasoft.mlrl.characters.PlayerHero;

import java.util.HashMap;
import java.util.Map;

public class RemoteScoreRecorder extends ScoreRecorder {

    @Override
    protected void record(long seed, PlayerHero player, Score score) {
        final Map<String, String> content = new HashMap<>();
        content.put("seed", String.valueOf(seed));
        content.put("player_class", score.player.klass.name());
        content.put("max_floor", String.valueOf(score.floor));
        content.put("killed_by", score.killer.name);
        content.put("runtime", String.valueOf(score.runTime));
        final HttpRequestBuilder builder = new HttpRequestBuilder();
        final Net.HttpRequest request = builder
                .newRequest()
                .method(Net.HttpMethods.POST)
                .url("https://www.peritasoft.com/cgi-bin/mlrl-score")
                .formEncodedContent(content)
                .build();
        Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                Gdx.app.log("ScoreRecorder", "Received HTTP Response: " +
                        httpResponse.getStatus().getStatusCode());
            }

            @Override
            public void failed(Throwable t) {
                Gdx.app.error("ScoreRecorder", "failed to send HTTP request", t);
            }

            @Override
            public void cancelled() {
                Gdx.app.log("ScoreRecorder", "HTTP request cancelled");
            }
        });
    }
}
