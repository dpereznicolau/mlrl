/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.scores;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LocalScoreRecorder extends ScoreRecorder {
    private static final String HIGH_SCORES = "high-scores";
    private static final String MAX_FLOOR = "max-floor";
    private static final String KILLER = "killer";
    private static final String UNLOCKED_ARCHER = "unlocked-archer";
    private static final String UNLOCKED_MAGE = "unlocked-mage";
    private static final String UNLOCKED_ENDLESS = "unlocked-endless";

    private final Json json;
    private final List<Score> highScores;
    private final Preferences preferences;

    public LocalScoreRecorder(Preferences preferences, Json json) {
        this.preferences = preferences;
        this.json = json;
        this.highScores = loadHighScores();
    }

    @Override
    protected void record(long seed, PlayerHero player, Score score) {
        int maxFloor = score.floor;
        if (maxFloor >= getMaxFloor(player)) {
            preferences.putInteger(buildKey(player, MAX_FLOOR), maxFloor);
            if (score.killer != null) {
                preferences.putString(buildKey(player, KILLER), score.killer.name);
            } else {
                preferences.remove(buildKey(player, KILLER));
            }
        }
        highScores.add(score);
        Collections.sort(highScores);
        while (highScores.size() > 50) {
            highScores.remove(0);
        }
        final String highScoresString = json.toJson(highScores);
        preferences.putString(HIGH_SCORES, highScoresString);
        preferences.flush();
    }

    @Override
    public void killed(Character c) {
        switch (c.getDemography()) {
            case TRASGA_HAMELIN:
                unlockArcher();
                break;
            case URMUK:
                unlockMage();
                break;
            case DEATH:
                unlockEndless();
                break;
        }
    }

    private String buildKey(PlayerHero playerHero, String subkey) {
        return (playerHero.getDemography().klass.name() + "-" + playerHero.getSex().name() + "-" + subkey).toLowerCase();
    }

    public int getMaxFloor(PlayerHero player) {
        return preferences.getInteger(buildKey(player, MAX_FLOOR), -1);
    }

    public String getKillerName(PlayerHero player) {
        return preferences.getString(buildKey(player, KILLER), "");
    }

    public boolean isArcherUnlocked() {
        return preferences.getBoolean(UNLOCKED_ARCHER, false);
    }

    private void unlockArcher() {
        preferences
                .putBoolean(UNLOCKED_ARCHER, true)
                .flush();
    }


    public boolean isMageUnlocked() {
        return preferences.getBoolean(UNLOCKED_MAGE, false);
    }

    private void unlockMage() {
        preferences
                .putBoolean(UNLOCKED_MAGE, true)
                .flush();
    }

    public boolean isEndlessUnlocked() {
        return preferences.getBoolean(UNLOCKED_ENDLESS, false);
    }

    private void unlockEndless() {
        preferences
                .putBoolean(UNLOCKED_ENDLESS, true)
                .flush();
    }

    public List<Score> getHighScores() {
        return highScores;
    }

    private List<Score> loadHighScores() {
        final String highScoresString = preferences.getString(HIGH_SCORES);
        if (highScoresString == null || highScoresString.isEmpty()) {
            return new ArrayList<>();
        }
        try {
            return json.fromJson(ArrayList.class, Score.class, highScoresString);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }


}
