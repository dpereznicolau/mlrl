/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.scores;

import com.badlogic.gdx.utils.TimeUtils;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEventAdapter;

import java.util.Date;

public abstract class ScoreRecorder extends GameEventAdapter {
    private long seed;
    private long startTime;
    private long runTime;
    private int maxFloor;
    private boolean recorded;

    @Override
    public void newGame(long seed) {
        this.seed = seed;
        maxFloor = 0;
        startTime = TimeUtils.millis();
        recorded = false;
        setRunTime(0);
    }

    @Override
    public void changedFloor(int floor, Level level) {
        maxFloor = Math.max(maxFloor, floor);
    }

    @Override
    public void died(PlayerHero player) {
        recordScore(player);
    }

    private void recordScore(PlayerHero player) {
        if (recorded) {
            // Nothing to do.
            return;
        }
        recorded = true;
        accumulateRunTime();
        final Score score = new Score(player, maxFloor + 1, getCurrentTime(), getRunTime());
        record(seed, player, score);
    }

    @Override
    public void endGame(PlayerHero player) {
        recordScore(player);
    }

    protected abstract void record(long seed, PlayerHero player, Score score);

    @Override
    public void pauseGame() {
        accumulateRunTime();
    }

    @Override
    public void resumeGame(long seed) {
        this.seed = seed;
        startTime = TimeUtils.millis();
    }

    private void accumulateRunTime() {
        setRunTime(getRunTime() + TimeUtils.millis() - startTime);
    }

    public long getRunTime() {
        return runTime;
    }

    public void setRunTime(long runTime) {
        this.runTime = runTime;
    }

    private String getCurrentTime() {
        final Date date = new Date(TimeUtils.millis());
        return (date.getYear() + 1900)
                + "-" + twoDigitNumber(date.getMonth() + 1)
                + "-" + twoDigitNumber(date.getDate())
                + " " + twoDigitNumber(date.getHours())
                + ":" + twoDigitNumber(date.getMinutes())
                ;
    }

    private String twoDigitNumber(int number) {
        return number < 10 ? "0" + number : String.valueOf(number);
    }
}