/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.weapons.Bow;
import com.peritasoft.mlrl.weapons.Dagger;
import com.peritasoft.mlrl.weapons.Sword;
import com.peritasoft.mlrl.weapons.WeaponGenerator;

public class InventoryRandomizer {
    public static void randomizeInventory(Inventory inventory, Demography demography) {
        inventory.empty();
        switch (demography) {
            case HERO_WARRIOR:
                inventory.add(new Sword(1, 0, 0, 0, 2));
                inventory.add(new Dagger(0, 0, 2, 0, 2));
                break;
            case HERO_ARCHER:
                inventory.add(new Bow(6, 0, 1, 0, 0, 2));
                inventory.add(new Dagger(0, 0, 2, 0, 2));
                inventory.putArrows(35);
                break;
            case HERO_MAGE:
                inventory.add(WeaponGenerator.generateRandomGrimoire(2));
                inventory.add(new Dagger(0, 0, 2, 0, 2));
                break;
        }
        int rand = MathUtils.random(4);
        switch (rand) {
            case 0:
                //3 random potions
                inventory.add(new Potion());
                inventory.add(new Potion());
                inventory.add(new Potion());
                break;
            case 1:
                //2 random potions & scroll
                inventory.add(new Potion());
                inventory.add(new Potion());
                inventory.add(ItemGenerator.generateScroll(1));
                break;
            case 2:
                //2 random scrolls & potion
                inventory.add(new Potion());
                inventory.add(ItemGenerator.generateScroll(1));
                inventory.add(ItemGenerator.generateScroll(1));
                break;
            case 3:
                //3 random scrolls
                inventory.add(ItemGenerator.generateScroll(1));
                inventory.add(ItemGenerator.generateScroll(1));
                inventory.add(ItemGenerator.generateScroll(1));
                break;
            case 4:
                //1 potion & SpellGem
                inventory.add(WeaponGenerator.generateFireSpellgem(1));
                inventory.add(new Potion());
                break;
        }
    }
}
