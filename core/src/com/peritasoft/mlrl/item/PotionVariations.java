/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public class PotionVariations {
    private static Map<PotionColour, PotionType> potionVar;

    public PotionVariations() {
        potionVar = new HashMap<>();
        //randomize colours - effects
        PotionColour[] colours = PotionColour.values();
        ArrayList<PotionType> types = new ArrayList<>(EnumSet.allOf(PotionType.class));
        for (PotionColour col : colours) {
            int i = MathUtils.random(types.size() - 1);
            potionVar.put(col, types.get(i));
            types.remove(i);
        }
    }

    public PotionVariations(HashMap<PotionColour, PotionType> variations) {
        potionVar = variations;
    }

    public static PotionType getPotionType(PotionColour colour) {
        return potionVar.get(colour);
    }

    public static PotionColour getPotionColour(PotionType type) {
        for (PotionColour col : potionVar.keySet()) {
            if (getPotionType(col) == type) return col;
        }
        return PotionColour.BLACK;
    }
}
