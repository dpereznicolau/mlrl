/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

public enum PotionType {
    HEALTH("Potion of Health", "This potion can heal your wounds"),
    DISPEL("Potion of Dispell", "A potion to dispel all the harmful effects"),
    STRENGTH("Potion of Strength", "A potion with the power of flex and increase your muscles. Cheaper than a gym, but maybe a little bit risky"),
    DEXTRERY("Potion of Dextrety", "This potion makes you agile and faster, and probably increases your accuracy"),
    WISDOM("Potion of Wisdom", "A potion that makes you smart? Is that possible?"),
    TELEPORT("Potion of Teleport", "Just a blink and you appear somewhere else. Without a doubt it is a work of witchcraft");

    private final String name;
    private final String description;

    PotionType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
