/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

public enum ItemCategory {
    FIST,
    POTION_BLACK,
    POTION_BLUE,
    POTION_GREEN,
    POTION_PURPLE,
    POTION_RED,
    POTION_YELLOW,
    WEAPON_BOW,
    WEAPON_DAGGER,
    WEAPON_SWORD,
    WEAPON_QUARTERSTAFF,
    ARROWS,
    GEM_AMETHYST,
    GEM_GARNET,
    GEM_JADE,
    GEM_RUBY,
    GEM_SAPPHIRE,
    SCROLL,
    SCROLL_FIRE,
    SCROLL_ICE,
    SCROLL_PETRIFY,
    SCROLL_POISON,
    BOOK_BROWN,
}
