/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.peritasoft.mlrl.weapons.Bow;

import java.util.ArrayList;
import java.util.Iterator;

public class Inventory implements Iterable<Item> {
    private final ArrayList<Item> items;
    private int arrows;
    private final static int MAX_ITEMS = 20;

    public Inventory() {
        this(0, new ArrayList<Item>());
    }

    public Inventory(int arrows, final ArrayList<Item> items) {
        this.arrows = arrows;
        this.items = items;
        if (items.size() > MAX_ITEMS) {
            throw new IllegalArgumentException("Too many items for inventory");
        }
    }

    public boolean add(Item item) {
        if (item == null || inventoryFull()) return false;
        if (item.isUsable() || item.isEquipable()) {
            items.add(item);
            return true;
        } else if (item instanceof Arrow) {
            arrows = Math.min(99, arrows + ((Arrow) item).countArrows());
            return true;
        } else {
            return false;
        }
    }

    private boolean inventoryFull() {
        return items.size() == MAX_ITEMS;
    }

    @Override
    public Iterator<Item> iterator() {
        return items.iterator();
    }

    public Item get(int i) {
        if (items.isEmpty()) return null;
        return items.get(i);
    }

    public int remove(int i) {
        items.remove(i);
        return i > 0 ? i - 1 : 0;
    }

    public int remove(Item item) {
        int index = getIndex(item);
        if (index < 0) {
            return 0;
        }
        return remove(index);
    }

    public int size() {
        return items.size();
    }

    public int getIndex(Item item) {
        return items.indexOf(item);
    }

    public void empty() {
        items.clear();
        arrows = 0;
    }

    public boolean hasABow() {
        for (Item item : items) {
            if (item instanceof Bow) return true;
        }
        return false;
    }

    public int countArrows() {
        return arrows;
    }

    public void putArrows(int arrows) {
        this.arrows += arrows;
    }

    public void takeArrows(int arrows) {
        this.arrows -= arrows;
    }
}
