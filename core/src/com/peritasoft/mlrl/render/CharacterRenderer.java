/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.peritasoft.mlrl.dungeongen.Direction;

import java.util.HashMap;
import java.util.Map;

public class CharacterRenderer {

    private final float FRAME_DURATION = 0.5f;
    private final int FRAMES_PER_DIR = 2;
    private Map<Direction, Animation<Sprite>> animations;

    CharacterRenderer(TextureAtlas atlas, String kind) {
        animations = new HashMap<>();
        final int length = Direction.values().length - 1; // Ignore NONE
        for (int dir = 0; dir < length; dir++) {
            final Direction direction = Direction.values()[dir];
            animations.put(direction, new Animation<>(FRAME_DURATION,
                    TextureAtlasHelper.mustCreateSprite(atlas, kind, dir * FRAMES_PER_DIR),
                    TextureAtlasHelper.mustCreateSprite(atlas, kind, dir * FRAMES_PER_DIR + 1)
            ));
        }
    }

    public CharacterRenderer(Map<Direction, Animation<Sprite>> animations) {
        this.animations = animations;
    }

    public void render(SpriteBatch batch, int x, int y, Direction dir, float timer, float alpha) {
        Animation<Sprite> animation = animations.get(dir);
        Sprite sprite = animation.getKeyFrame(timer, true);
        sprite.setPosition(x, y);
        sprite.draw(batch, alpha);
    }
}
