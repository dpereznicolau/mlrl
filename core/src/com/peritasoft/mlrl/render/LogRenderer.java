/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.events.Log;

public class LogRenderer {
    private final SpriteBatch batch;
    private final BitmapFont font;
    private final int startX;
    private final int startY;

    private static final int FONT_HEIGHT = 8;

    public LogRenderer(SpriteBatch batch, BitmapFont font, int startX, int startY) {
        this.batch = batch;
        this.font = font;
        this.startX = startX;
        this.startY = startY;
    }

    public void render(Log log) {
        int numLines = Math.min(log.countLines(), 4);
        float y = startY - FONT_HEIGHT;
        for (int l = numLines; l > 0; l--) {
            final Log.Line line = log.getLine(l - 1);
            Color color = line.color;
            float alpha = (float) (numLines - l + 1) / (float) numLines;
            font.setColor(color.r, color.g, color.b, alpha);
            GlyphLayout layout = font.draw(batch,
                    line.repeated == 1 ? line.message : line.message + " (x" + line.repeated + ")",
                    startX, y,
                    MyLittleRogueLike.SCREENWIDTH - startX,
                    Align.left,
                    true);
            y -= (layout.height + font.getLineHeight() - font.getXHeight());
        }
    }
}
