/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Geometry;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.*;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.props.Prop;


public class GameRenderer {
    private static final int GLYPH_WIDTH = 16;
    private static final int GLYPH_HEIGHT = 16;
    private static final int START_Y = GLYPH_HEIGHT * 3;
    private static final int START_X = 0;
    private static final int GLYPHPERWIDTH = (MyLittleRogueLike.SCREENWIDTH / GLYPH_WIDTH);
    private static final int GLYPHPERHEIGHT = (MyLittleRogueLike.SCREENHEIGHT / GLYPH_HEIGHT) - 4;
    private float flicker = 1f;
    private float flickerDir = -1.5f;
    private boolean centerOnPlayer = true;

    private final RendererAtlas[] rendererAtlases;
    private RendererAtlas atlas;
    private final SpriteBatch batch;
    private final ShapeRenderer shapeRenderer;
    private float timer;
    private int firstRow;
    private int firstColumn;
    private final Position unprojected = new Position(0, 0);
    private boolean fov = true;
    private final Matrix4 originalTransform = new Matrix4();
    private final Matrix4 panMatrix = new Matrix4();


    public GameRenderer(SpriteBatch batch) {
        this.batch = batch;
        rendererAtlases = new RendererAtlas[]{
                new PrettyRendererAtlas(),
                new AsciiRendererAtlas(),
        };
        atlas = rendererAtlases[0];
        shapeRenderer = new ShapeRenderer();
    }

    public void render(Level level, PlayerHero player, Projectile projectile, float delta) {
        timer += delta;
        flicker += delta * flickerDir;
        if (flicker <= 0f) {
            flicker = 0.0f;
            flickerDir *= -1;
        } else if (flicker > 1f) {
            flicker = 1f;
            flickerDir *= -1;
        }

        if (hasFov()) {
            resetPan();
        } else if (centerOnPlayer) {
            resetPan();
            pan(MyLittleRogueLike.SCREENWIDTH / 2f - player.getPositionX() * GLYPH_WIDTH, MyLittleRogueLike.SCREENHEIGHT / 2f - (player.getPositionY() + 4) * GLYPH_HEIGHT);
        }

        originalTransform.set(batch.getTransformMatrix());
        batch.setTransformMatrix(panMatrix);

        computeStartCoordinates(player);
        renderLevel(level);
        renderLifeObjs(level, LifeObjLayer.UNDER);
        renderPlayer(player);
        renderProjectile(projectile);
        renderEnemies(level, player.getPosition(), player.getSightRadius());
        renderLifeObjs(level, LifeObjLayer.OVER);
        renderCrosshairs(player);
        batch.setTransformMatrix(originalTransform);
    }

    private void computeStartCoordinates(PlayerHero player) {
        firstColumn = hasFov() ? player.getPositionX() - GLYPHPERWIDTH / 2 : 0;
        firstRow = hasFov() ? player.getPositionY() - GLYPHPERHEIGHT / 2 : 0;
    }

    private boolean hasFov() {
        return fov;
    }

    private void renderPlayer(PlayerHero player) {
        if (!player.isDead()) {
            renderCharacter(player, 1f);
        }
    }

    private int rowToPixel(int y) {
        return START_Y + (y - firstRow) * GLYPH_HEIGHT;
    }

    private int pixelToRow(float y) {
        return (int) (y - START_Y + 0.5) / GLYPH_HEIGHT + firstRow;
    }

    private int colToPixel(int x) {
        return START_X + (x - firstColumn) * GLYPH_WIDTH;
    }

    private int pixelToCol(float x) {
        return (int) (x - START_X + 0.5) / GLYPH_WIDTH + firstColumn;
    }

    private void renderCrosshairs(PlayerHero player) {
        final Crosshairs crosshairs = player.getCrosshairs();
        if (crosshairs == null) return;
        Position p = new Position(player.getPosition());
        batch.end();
        drawLine(p, crosshairs.getPosition());
        batch.begin();
    }

    private void drawLine(Position from, Position to) {
        Geometry.walkLine(from, to, new Geometry.LineWalker() {
            @Override
            public boolean walk(Position p) {
                markTile(p);
                return true;
            }
        });
    }

    private void markTile(Position p) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
        shapeRenderer.setColor(1f, 0f, 0f, 0.4f);
        shapeRenderer.rect(colToPixel(p.getX()), rowToPixel(p.getY()), GLYPH_WIDTH, GLYPH_HEIGHT);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    private void renderProjectile(Projectile projectile) {
        if (projectile == null) return;
        ProjectileRenderer renderer = atlas.getProjectile(projectile.getProjectileType());
        renderer.render(batch, colToPixel(projectile.getPosition().getX()) + projectile.getOffsetX(), rowToPixel(projectile.getPosition().getY()) + projectile.getOffsetY(), angle2Way(projectile.getAngle()));
    }

    private EightWay angle2Way(float angle) {
        if ((angle >= 0 && angle <= 22.5)) {
            return EightWay.E;
        }
        if (angle >= 22.5 && angle <= 67.5) {
            return EightWay.NE;
        }
        if (angle >= 67.5 && angle <= 112.5) {
            return EightWay.N;
        }
        if (angle >= 112.5 && angle <= 157.5) {
            return EightWay.NW;
        }
        if (angle >= 157.5 && angle <= 202.5) {
            return EightWay.W;
        }
        if (angle >= 202.5 && angle <= 247.5) {
            return EightWay.SW;
        }
        if (angle >= 247.5 && angle <= 292.5) {
            return EightWay.S;
        }
        if (angle >= 292.5 && angle <= 337.5) {
            return EightWay.SE;
        }
        return EightWay.E;
    }

    private void renderLevel(Level level) {
        TerrainRenderer terrain = atlas.getTerrain(level.getType());
        for (int y = 0; y < (hasFov() ? GLYPHPERHEIGHT : level.getHeight()); y++) {
            final int row = firstRow + y;
            final int pixelY = (y * GLYPH_HEIGHT) + START_Y;
            for (int x = 0; x < (hasFov() ? GLYPHPERWIDTH : level.getWidth()); x++) {
                final int col = firstColumn + x;
                final int pixelX = (x * GLYPH_WIDTH) + START_X;
                final Cell cell = level.getCell(col, row);
                float alpha = getVisibility(cell);
                terrain.render(batch, pixelX, pixelY, cell.tile, cell.wallJoint, cell.variation, alpha, timer);
                Item item = cell.getItem();
                if (item != null) {
                    Sprite sprite = atlas.getItem(item.getCategory());
                    sprite.setPosition(pixelX, pixelY);
                    sprite.draw(batch, alpha);
                }
            }
        }
        for (int y = (hasFov() ? GLYPHPERHEIGHT : level.getHeight()) - 1; y > 0; y--) {
            final int row = firstRow + y;
            final int pixelY = (y * GLYPH_HEIGHT) + START_Y;
            for (int x = 0; x < (hasFov() ? GLYPHPERWIDTH : level.getWidth()); x++) {
                final int col = firstColumn + x;
                final Cell cell = level.getCell(col, row);
                Prop prop = cell.getProp();
                if (prop != null) {
                    final int pixelX = (x * GLYPH_WIDTH) + START_X;
                    float alpha = getVisibility(cell);
                    PropRenderer propRenderer = atlas.getProp(prop.getType());
                    propRenderer.render(batch, pixelX, pixelY, alpha, timer);
                }
            }
        }
    }

    private void renderLifeObjs(Level level, LifeObjLayer layer) {
        for (LifeObj l : level.getLifeObjs()) {
            if (l.getLayer() == layer &&
                    (!hasFov() ||
                            (l.getPositionX() >= firstColumn && l.getPositionX() <= firstColumn + GLYPHPERWIDTH &&
                                    l.getPositionY() >= firstRow && l.getPositionY() <= firstRow + GLYPHPERHEIGHT))) {
                int x = l.getPositionX();
                int y = l.getPositionY();
                final Cell cell = level.getCell(x, y);
                float alpha = getVisibility(cell);
                switch (l.getType()) {
                    case POISON: {
                        if (level.isWalkable(x, y)) {
                            AreaEffectRenderer areaEffectRenderer = atlas.getAreaEffectRenderer(l.getType());
                            areaEffectRenderer.render(batch, colToPixel(x), rowToPixel(y), l.getLifeTime());
                        }
                    }
                    break;
                    case MAGIC_CIRCLE:
                    case BONES:
                    case ENEMY_DEAD:
                    case PLAYER_DEAD: {
                        ObjectRenderer object = atlas.getObject(l.getType());
                        object.render(batch, colToPixel(x), rowToPixel(y), cell.variation, alpha);
                    }
                    break;
                    case WEB: {
                        ObjectRendererOffset object = atlas.getObjectOffset(l.getType());
                        Web w = (Web) l;
                        if (w.getVariation() == 0 || w.getVariation() == 1) {
                            object.setOffsetY(8);
                        } else {
                            object.setOffsetY(0);
                        }
                        object.render(batch, colToPixel(x), rowToPixel(y), w.getVariation(), alpha);
                    }
                    break;
                    case IMPACT: {
                        ImpactRenderer impactRenderer = atlas.getImpactRenderer(((Impact) l).getProjectileType());
                        impactRenderer.render(batch, colToPixel(x), rowToPixel(y), l.getLifeTime());
                    }
                    break;
                    case MISS:
                    case LVLUP: {
                        StatusFxRenderer miss = atlas.getStatusFxRenderer(l.getType());
                        miss.render(batch, colToPixel(x), rowToPixel(y), l.getLifeTime(), alpha);
                    }
                    break;
                    case FLOATING_NUMBER: {
                        final FloatingNumberRenderer numberRenderer = atlas.getFloatingNumberRenderer();
                        final FloatingNumber number = (FloatingNumber) l;
                        numberRenderer.render(batch, number, colToPixel(x), rowToPixel(y), alpha);
                    }
                    break;
                    case CUT: {
                        ImpactRenderer impactRenderer = atlas.getAttackRenderer();
                        impactRenderer.render(batch, colToPixel(x), rowToPixel(y), l.getLifeTime());
                    }
                    break;
                    case FIRE:
                    case ICE:
                    case LIGHTNING:
                    case VOID: {
                        AreaEffectRenderer areaEffectRenderer = atlas.getAreaEffectRenderer(l.getType());
                        areaEffectRenderer.render(batch, colToPixel(x), rowToPixel(y), l.getLifeTime());
                    }
                    break;
                }
            }
        }
    }

    private void renderEnemies(Level level, Position center, int radius) {
        if (!hasFov() || !level.hasFov()) {
            radius = Math.max(level.getHeight(), level.getWidth());
        }
        for (int y = -radius; y <= radius; y++) {
            final int row = center.getY() + y;
            for (int x = -radius; x <= radius; x++) {
                if (y == 0 && x == 0) continue;
                final int col = center.getX() + x;
                Cell cell = level.getCell(col, row);
                if (cell.hasCharacter()) {
                    renderCharacter(cell.getCharacter(), isInFOV(cell) ? getVisibility(cell) : 0f);
                }
            }
        }
    }

    private float getVisibility(Cell cell) {
        if (!hasFov()) return 1f;
        final float visibility = cell.getVisibility();
        final float fovRange = cell.getFovRange();
        if (fovRange >= 1f || fovRange == 0f) return visibility;
        return MathUtils.lerp((float) (1.0 - (1 - visibility) / fovRange), visibility, flicker);
    }

    private boolean isInFOV(Cell cell) {
        return !hasFov() || cell.isInFOV();
    }

    private void renderCharacter(Character c, float alpha) {
        CharacterRenderer sprite = atlas.getCharacter(c.getDemography());
        int offsetX = c.isAttacking() ? c.getLastDirection().x * 4 : 0;
        int offsetY = c.isAttacking() ? c.getLastDirection().y * 4 : 0;
        sprite.render(batch, colToPixel(c.getPositionX()) + offsetX, rowToPixel(c.getPositionY()) + offsetY, c.getLastDirection(), timer, alpha);
        if (c.isConfused()) {
            StatusFxRenderer confused = atlas.getStatusFxRenderer(LifeObjType.CONFUSED);
            confused.render(batch, colToPixel(c.getPositionX()), rowToPixel(c.getPositionY()), timer, alpha);
        }
        if (c.isPetrified()) {
            StatusFxRenderer petrified = atlas.getStatusFxRenderer(LifeObjType.PARALYZED);
            petrified.render(batch, colToPixel(c.getPositionX()), rowToPixel(c.getPositionY()), timer, alpha);
        }
        if (c.isFrozen()) {
            StatusFxRenderer frozen = atlas.getStatusFxRenderer(LifeObjType.FROZEN);
            frozen.render(batch, colToPixel(c.getPositionX()), rowToPixel(c.getPositionY()), timer, alpha);
        }
    }


    public void dispose() {
        for (RendererAtlas renderer : rendererAtlases) {
            renderer.dispose();
        }
    }

    public boolean isAtlasGraphical() {
        return atlas instanceof PrettyRendererAtlas;
    }

    public void switchAtlas() {
        System.arraycopy(rendererAtlases, 1, rendererAtlases, 0, rendererAtlases.length - 1);
        rendererAtlases[rendererAtlases.length - 1] = atlas;
        atlas = rendererAtlases[0];
    }

    public Position unproject(Level level, PlayerHero player, float x, float y) {
        computeStartCoordinates(player);
        unprojected.set(pixelToCol(x - panMatrix.val[Matrix4.M03]), pixelToRow(y - panMatrix.val[Matrix4.M13]));
        return unprojected;
    }

    public void enableFieldOfView(boolean enable) {
        this.fov = enable;
    }

    public void enableCenterOnPlayerOnce(boolean enable) {
        centerOnPlayer = enable;
    }

    public void pan(float deltaX, float deltaY) {
        panMatrix.translate(deltaX, deltaY, 0f);
    }

    public void resetPan() {
        panMatrix.idt();
    }
}
