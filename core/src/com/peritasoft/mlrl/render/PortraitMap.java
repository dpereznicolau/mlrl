/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.characters.Sex;

public class PortraitMap implements Disposable {

    private final TextureAtlas atlas;
    private final TextureAtlas.AtlasRegion[][] portraits;

    public PortraitMap() {
        atlas = new TextureAtlas("td_portraits.atlas");
        portraits = new TextureAtlas.AtlasRegion[2][3];
        portraits[Sex.FEMALE.ordinal()][Demography.HERO_WARRIOR.ordinal()] = atlas.findRegion("warrior_f1");
        portraits[Sex.FEMALE.ordinal()][Demography.HERO_ARCHER.ordinal()] = atlas.findRegion("archer_f1");
        portraits[Sex.FEMALE.ordinal()][Demography.HERO_MAGE.ordinal()] = atlas.findRegion("mage_f1");
        portraits[Sex.MALE.ordinal()][Demography.HERO_WARRIOR.ordinal()] = atlas.findRegion("warrior_m1");
        portraits[Sex.MALE.ordinal()][Demography.HERO_ARCHER.ordinal()] = atlas.findRegion("archer_m1");
        portraits[Sex.MALE.ordinal()][Demography.HERO_MAGE.ordinal()] = atlas.findRegion("mage_m1");
    }

    public void dispose() {
        atlas.dispose();
    }

    public TextureAtlas.AtlasRegion get(Sex sex, Demography demography) {
        return portraits[sex.ordinal()][demography.ordinal()];
    }
}
