/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Align;
import com.peritasoft.mlrl.effects.FloatingNumber;

public class TextureFloatingNumberRenderer implements FloatingNumberRenderer {
    private static final int Y_OFFSET = 13;
    private static final int NEXT_Y = 8;
    private static final int TARGET_WIDTH = 16;
    final BitmapFont font;

    public TextureFloatingNumberRenderer() {
        font = new BitmapFont(Gdx.files.internal("3x5.fnt"));
    }

    @Override
    public void render(SpriteBatch batch, FloatingNumber floatingNumber, int x, int y, float alpha) {
        final Color color = floatingNumber.getNumberType() == FloatingNumber.Type.DAMAGE ? Color.ORANGE : Color.GREEN;
        font.setColor(color);

        y = (int) MathUtils.lerp(y, y + NEXT_Y, floatingNumber.getLifeTime() / floatingNumber.getTimeToLive());
        font.draw(batch, String.valueOf(floatingNumber.getNumber()), x, y + Y_OFFSET, TARGET_WIDTH, Align.center, false);
    }
}
