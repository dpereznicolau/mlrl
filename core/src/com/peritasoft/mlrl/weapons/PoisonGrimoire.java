/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.Poison;
import com.peritasoft.mlrl.effects.ProjectileType;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class PoisonGrimoire extends MagicWeapon implements Weapon, Shootable, Grimoire {

    private final int manaCost;
    private final int minDistance;

    public PoisonGrimoire(int minWis, int manaCost, int minDistance, int bonusDamage) {
        super("Poison Grimoire",
                "A putrid book that emanates a green fetid cloud. It seems that for its correct use a minimum of wisdom is needed (" + minWis + ")",
                0, 0, 0, 0, bonusDamage, minWis);
        this.manaCost = manaCost;
        this.minDistance = minDistance;
    }

    @Override
    public PoisonGrimoire copy() {
        return new PoisonGrimoire(getMinWis(), getManaCost(), getMinDistance(), getBonusDamage());
    }

    @Override
    public Ammo getAmmo() {
        return null;
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.BOOK_BROWN;
    }

    @Override
    public int getRange() {
        return Math.min(Math.max(owner.getWis(), minDistance), manaCost + owner.getMp());
    }

    @Override
    public boolean canShoot(Character shooter) {
        if (shooter.getWis() < getMinWis()) {
            GameEvent.notEnoughWis();
            return false;
        } else if (shooter.getMp() < manaCost) {
            GameEvent.notEnoughMana(shooter);
            return false;
        }
        return true;
    }

    @Override
    public ProjectileType shoot(Character shooter, Position target, Level level) {
        int dist = target.distance(shooter.getPosition());
        shooter.spendMana(manaCost + Math.max(dist - minDistance, 0));
        return ProjectileType.POISONBALL;
    }

    @Override
    public void attack(Character attacker, Character target, Level level) {
        if (MathUtils.random(1, 20) + attacker.getDex() > MathUtils.random(1, 20) + target.getDex()) {
            attack(attacker, target, attacker.getStr());
        } else {
            GameEvent.attackMissed(attacker, target);
        }
    }


    private void attack(Character attacker, Character target, int dmg) {
        GameEvent.attackHit(attacker, target, dmg, getCategory());
        target.receiveHit(dmg, attacker);
    }

    public void miss(Character shooter, Level level, Position pos) {
        envenom(shooter, level, pos);
    }

    @Override
    public void impact(Character shooter, Character target, Level level) {
        int shooterWis = shooter.getWis();
        if (shooter.isConfused()) {
            shooterWis = shooter.getWis() / 2;
        }
        int evadeBonus = (target.getDex() + target.getWis()) / 2;
        if (MathUtils.random(1, 20) + shooterWis > MathUtils.random(1, 20) + evadeBonus) {
            attack(shooter, target, getDamage(shooter));
            envenom(shooter, level, target.getPosition());
        } else {
            envenom(shooter, level, target.getPosition());
            GameEvent.attackMissed(shooter, target);
        }
    }

    private void envenom(Character shooter, Level level, Position position) {
        for (Direction direction : Direction.values()) {
            level.getLifeObjs().add(new Poison(rollBonusDamage(), level, new Position(position, direction), shooter, 2));
        }
    }

    private int getDamage(Character shooter) {
        return (int) (shooter.getWis() * 0.8f);
    }

    @Override
    public int getMinDistance() {
        return minDistance;
    }

    @Override
    public int getManaCost() {
        return manaCost;
    }
}
