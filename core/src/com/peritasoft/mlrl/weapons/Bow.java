/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.ProjectileType;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Arrow;
import com.peritasoft.mlrl.item.ItemCategory;

public class Bow extends RangedWeapon implements Weapon, Shootable {

    private final int range;

    public Bow(int range, int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
        super("Bow", "A simply Bow", bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
        this.range = range;
        this.description = getRandomDescription();
    }

    @Override
    public Bow copy() {
        return new Bow(range, getBonusStr(), getBonusDex(), getBonusCon(), getBonusWis(), getBonusDamage());
    }

    @Override
    public void attack(Character attacker, Character target, Level level) {
        if (MathUtils.random(1, 20) + attacker.getDex() > MathUtils.random(1, 20) + target.getDex()) {
            attack(attacker, target, attacker.getStr() + rollBonusDamage());
        } else {
            GameEvent.attackMissed(attacker, target);
        }
    }

    private void attack(Character attacker, Character target, int dmg) {
        GameEvent.attackHit(attacker, target, dmg, getCategory());
        target.receiveHit(dmg, attacker);
    }

    public void miss(Character shooter, Level level, Position pos) {
        level.dropItem(new Arrow(), pos);
    }

    @Override
    public void impact(Character shooter, Character target, Level level) {
        int distance = shooter.distance(target);
        if (distance <= range && MathUtils.random(1, 20) + shooter.getDex() + getAttackPenalty(distance) > MathUtils.random(1, 20) + target.getDex()) {
            int dmg = ((shooter.getStr() + shooter.getDex()) / 2) + rollBonusDamage();
            attack(shooter, target, dmg);
        } else {
            GameEvent.attackMissed(shooter, target);
        }
    }

    public boolean canShoot(Character shooter) {
        return shooter.countArrows() > 0;
    }

    @Override
    public ProjectileType shoot(Character shooter, Position target, Level level) {
        shooter.shootArrow();
        return ProjectileType.ARROW;
    }

    @Override
    public Ammo getAmmo() {
        return new Ammo(ItemCategory.ARROWS, owner.countArrows());
    }


    private int getAttackPenalty(int targetDistance) {
        return (range / 2 <= targetDistance ? 0 : -2);
    }

    private String getRandomDescription() {
        String[] desc = {"This popular recurve bow has been properly constructed of durable blackwood.",
                "Its string is made from a very uncommon material around these parts of the world.",
                "The limbs have been decorated with ivory details and end in rounded curves ornamented with magic runes.",
                "The handle is wrapped in ebon and decorated with leaves.",
                "This intricate long bow has been superbly constructed of only the finest walnut wood.",
                "Its string is made from exceptional cotton, it's a rare material around these parts of the world.",
                "The limbs have been decorated with gilded details and end in points ornamented with teeth.",
                "The handle is wrapped in thick leather and decorated with small gems.",
                "This elegant shortbow has been superbly constructed of amazingly strong rosewood.",
                "Its string is made from favorable silk, it's an extremely rare material.",
                "The limbs have been decorated with rows of gems and end in slight curves ornamented with long spikes.",
                "The handle is wrapped in soft hide.",
                "This elegant composite bow has been expertly constructed of common steel.",
                "Its string is made from superior bear gut, it's a fairly uncommon material around these parts of the world.",
                "The limbs have been decorated with carefully wrapped leather and end in long points ornamented with spikes.",
                "The handle is wrapped in hide and decorated with rune-like symbols.",
                "This masterfully crafted recurve bow has been cleverly constructed of durable onyx.",
                "Its string is made from pristine boar hide, it's a very common material.",
                "The limbs have been decorated with golden rings and end in slight curves ornamented with animal scales.",
                "The handle is wrapped in rare silk and decorated with intricate thread work.",
                "This superior flatbow has been carefully constructed of decent osage.",
                "Its string is made from prime deer hide.",
                "The limbs have been decorated with silken ribbons and end in narrow points ornamented with magic runes.",
                "The handle is wrapped in cloth and decorated with gilded linings.",
                "This well designed shortbow has been cleverly constructed of heavy-duty ironoak.",
                "Its string is made from excellent quality dacron, an uncommon material.",
                "The limbs have been decorated with a thin layer of leather and end in long points ornamented with crafted talons.",
                "The handle is wrapped in fur and decorated with magic runes.",
                "This beautiful reflex bow has been admirably constructed of long-lasting ipe wood.",
                "Its string is made from decent quality deer sinew.",
                "The limbs have been decorated with many small spikes and end in narrow points shaped like branches.",
                "The handle is wrapped in ivory and decorated with ominous carvings.",
                "This exceptional composite bow has been adeptly constructed of common blackwood.",
                "Its string is made from excellent quality deer sinew.",
                "The limbs have been decorated with ivory details and end in rounded curves ornamented with crafted leaves.",
                "The handle is wrapped in thick leather and decorated with tribal paintings.",
                "This ornate composite bow has been masterfully constructed of exceptional adamantium.",
                "Its string is made from valuable silk.",
                "The limbs have been decorated with seemingly glowing runes and end in long points ornamented with sharp blades.",
                "The handle is wrapped in linen and decorated with intricate thread work.",
                "This rare shortbow has been superbly constructed of valuable ipe wood.",
                "Its string is made from first-rate dragon sinew.",
                "The limbs have been decorated with several long spikes and end in narrow points ornamented with thin glowing ribbons.",
                "The handle is wrapped in rare leather and decorated with intricate thread work.",
                "This marvelous composite bow has been rigorously constructed of only the finest walnut wood.",
                "Its string is made from fine boar sinew.",
                "The limbs have been decorated with animal bones and end in slight curves shaped like tusks.",
                "The handle is wrapped in ivory and decorated with rune-like symbols.",
                "This beautiful longbow has been well constructed of decent onyx.",
                "Its string is made from splendid cotton.",
                "The limbs have been decorated with magical runes and end in rounded curves ornamented with long spikes.",
                "The handle is wrapped in dark leather and decorated with ominous carvings.",
                "This well-made flatbow has been carefully constructed of exceptional osage.",
                "Its string is made from pristine grass fiber.",
                "The limbs have been decorated with curved talon-like spikes and end in rounded curves shaped like scythes.",
                "The handle is wrapped in rare hide and decorated with sigil decorations.",
                "This masterfully crafted compound bow has been flawlessly constructed of amazingly strong bamboo.",
                "Its string is made from decent quality deer sinew.",
                "The limbs have been decorated with rows of small teeth and end in long points ornamented with crafted leaves.",
                "The handle is wrapped in cloth and decorated with intricate thread work.",
                "This nifty recurve bow has been delicately constructed of amazingly strong maple.",
                "Its string is made from pristine deer hide.",
                "The limbs have been decorated with carefully wrapped leather and end in points ornamented with wrapped leather.",
                "The handle is wrapped in light leather and decorated with carved woodworks.",
                "This superior longbow has been masterfully constructed of valuable obsidian.",
                "Its string is made from valuable hemp, it's a fairly uncommon material around these parts of the world.",
                "The limbs have been decorated with small sigils and end in points ornamented with small sigils.",
                "The handle is wrapped in dark leather and decorated with sigil decorations.",
                "This marvelous self bow has been superbly constructed of reinforced adamantium.",
                "Its string is made from decent quality bear gut.",
                "The limbs have been decorated with magical runes and end in narrow points ornamented with wrapped leather.",
                "The handle is wrapped in reptilian skin and decorated with tribal paintings.",
                "This nifty longbow has been efficiently constructed of uncommon osage.",
                "Its string is made from deluxe dragon sinew.",
                "The limbs have been decorated with wing-like constructions and end in curves shaped like wings.",
                "The handle is wrapped in coarse hide and decorated with tribal paintings.",
                "This first-rate flatbow has been reliably constructed of heavy-duty willow.",
                "Its string is made from great quality linen.",
                "The limbs have been decorated with wing-like constructions and end in points ornamented with obsidian tips.",
                "The handle is wrapped in linen and decorated with symbolic writing."};
        return desc[MathUtils.random(desc.length - 1)];
    }

    @Override
    public int getRange() {
        return range;
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.WEAPON_BOW;
    }
}
