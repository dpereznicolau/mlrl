/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.ProjectileType;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class VoidBall implements Shootable {
    final int dmg;
    final int manaCost;
    final float confuseChance;

    public VoidBall(int dmg, int manaCost, float confuseChance) {
        this.dmg = dmg;
        this.manaCost = manaCost;
        this.confuseChance = confuseChance;
    }


    @Override
    public int getRange() {
        return 6;
    }

    @Override
    public boolean canShoot(Character shooter) {
        if (shooter.getMp() < manaCost) {
            GameEvent.notEnoughMana(shooter);
            return false;
        }
        return true;
    }

    @Override
    public ProjectileType shoot(Character shooter, Position target, Level level) {
        shooter.spendMana(manaCost);
        return ProjectileType.VOIDBALL;
    }

    @Override
    public void miss(Character shooter, Level level, Position pos) {

    }

    @Override
    public void impact(Character shooter, Character target, Level level) {
        int shooterWis = shooter.getWis();
        if (shooter.isConfused()) {
            shooterWis = shooter.getWis() / 2;
        }
        int evadeBonus = (target.getDex() + target.getWis()) / 2;
        if (MathUtils.random(1,20) + shooterWis > MathUtils.random(1,20) + evadeBonus) {
            GameEvent.attackHit(shooter, target, dmg, ItemCategory.ARROWS);
            target.receiveHit(dmg, shooter);
            if (MathUtils.randomBoolean(confuseChance)) {
                target.confuse();
            }
        } else {
            GameEvent.attackMissed(shooter, target);
        }
    }

    @Override
    public Ammo getAmmo() {
        return null;
    }
}
