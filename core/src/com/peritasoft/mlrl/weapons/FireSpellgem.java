/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.ProjectileType;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class FireSpellgem extends RangedWeapon implements Weapon {

    private int uses;
    private final int baseDamage;
    private final float impactChance;

    public FireSpellgem(int uses, int baseDamage, int bonusDamage, float impactChance) {
        super("Fire Spellgem",
                "A gem glowing in red. With a close look you can see a little spark on it core. Seems like you can use it "
                        + uses + " times before his glow fades out.",
                0, 0, 0, 0, bonusDamage);
        this.uses = uses;
        this.baseDamage = baseDamage;
        this.impactChance = impactChance;
    }

    @Override
    public FireSpellgem copy() {
        return new FireSpellgem(uses, baseDamage, getBonusDamage(), impactChance);
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.GEM_RUBY;
    }

    @Override
    public void attack(Character attacker, Character target, Level level) {
        if (MathUtils.random(1, 20) + attacker.getDex() > MathUtils.random(1, 20) + target.getDex()) {
            attack(attacker, target, attacker.getStr() + baseDamage);
        } else {
            GameEvent.attackMissed(attacker, target);
        }
        if (MathUtils.randomBoolean()) {
            attacker.getInventory().remove(attacker.getInventory().getIndex(this));
            attacker.disarm();
            GameEvent.fireSpellgemCracked();
        }
    }

    private int getDamage() {
        return baseDamage + rollBonusDamage();
    }

    private void attack(Character attacker, Character target, int dmg) {
        GameEvent.attackHit(attacker, target, dmg, getCategory());
        target.receiveHit(dmg, attacker);
    }

    public void miss(Character shooter, Level level, Position pos) {
        blast(shooter, level, pos);
    }

    @Override
    public void impact(Character shooter, Character target, Level level) {
        if (MathUtils.randomBoolean(impactChance)) {
            if (target.isFlamable()) {
                attack(shooter, target, getDamage());
                target.setRegenHp(false);
            }
            blast(shooter, level, target.getPosition());
        } else {
            GameEvent.attackMissed(shooter, target);
        }
    }

    private void blast(Character attacker, Level level, Position pos) {
        int radius = 1;
        FireNova blast = new FireNova(radius, getDamage() / 4, getCategory());
        blast.cast(attacker, level, pos);
    }

    public int getBaseDamage() {
        return baseDamage;
    }

    public int getUses() {
        return uses;
    }

    public float getImpactChance() {
        return impactChance;
    }

    @Override
    public boolean canShoot(Character shooter) {
        return uses > 0;
    }

    @Override
    public ProjectileType shoot(Character shooter, Position target, Level level) {
        uses--;
        return ProjectileType.FIREBALL;
    }

    @Override
    public Ammo getAmmo() {
        return new Ammo(ItemCategory.GEM_RUBY, uses);
    }


    @Override
    public int getRange() {
        return 8;
    }

}
