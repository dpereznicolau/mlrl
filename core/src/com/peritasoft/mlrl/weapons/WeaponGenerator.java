/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.item.KindOfWeapon;

public class WeaponGenerator {

    public static Sword generateSword(int level) {
        int bonusStr = MathUtils.random(level / 4, level / 2);
        int bonusDex = MathUtils.random(-level / 2, level / 2);
        int bonusCon = MathUtils.random(-level / 2, level - (level / 4));
        int bonusWis = MathUtils.random(-level / 2, level / 2);
        int bonusDamage = MathUtils.random(2, Math.max(level, 2));

        return new Sword(bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
    }

    public static Lance generateLance(int level) {
        int bonusStr = MathUtils.random(level / 2, level);
        int bonusDex = MathUtils.random(-level, -level / 2);
        int bonusCon = MathUtils.random(-level / 2, level / 2);
        int bonusWis = MathUtils.random(-level, level / 2);
        int bonusDamage = MathUtils.random(3, Math.max(level, 3));

        return new Lance(bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
    }

    public static Bow generateBow(int level) {
        int range = MathUtils.random(5, 8);
        int bonusStr = MathUtils.random(-level / 4, level / 4);
        int bonusDex = MathUtils.random(level / 4, level / 2);
        int bonusCon = MathUtils.random(-level / 2, level - (level / 4));
        int bonusWis = MathUtils.random(-level / 2, level / 2);
        int bonusDamage = MathUtils.random(2, Math.max(level, 2));

        return new Bow(range, bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
    }

    public static Dagger generateDagger(int level) {
        int bonusStr = MathUtils.random(-level / 2, level / 2);
        int bonusDex = MathUtils.random(level / 4, level / 2);
        int bonusCon = MathUtils.random(-level / 2, level - (level / 4));
        int bonusWis = MathUtils.random(-level / 2, level / 2);
        int bonusDamage = MathUtils.random(2, Math.max(level, 2));

        return new Dagger(bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
    }

    public static FireSpellgem generateFireSpellgem(int level) {
        int uses = MathUtils.random(5, 25);
        int baseDamage = Math.max(4, level);
        int bonusDamage = MathUtils.random(Math.max(1, level - 4), level);
        float impactChance = Math.max(0.5f, Math.min(MathUtils.log(10000f, level + 1) * 2.5f, 0.99f));
        return new FireSpellgem(uses, baseDamage, bonusDamage, impactChance);
    }

    public static FireGrimoire generateFireGrimoire(int level) {
        int minDistance = MathUtils.random(3, 6);
        int bonusDamage = MathUtils.random(3, Math.max(level / 2, 3));
        int manaCost = (bonusDamage + 1);

        return new FireGrimoire(level, manaCost, minDistance, bonusDamage);
    }

    public static PoisonGrimoire generatePoisonGrimoire(int level) {
        int minDistance = MathUtils.random(3, 6);
        int bonusDamage = MathUtils.random(3, Math.max(level / 2, 3));
        int manaCost = (bonusDamage + 1);

        return new PoisonGrimoire(level, manaCost, minDistance, bonusDamage);
    }

    public static IceGrimoire generateIceGrimoire(int level) {
        int minDistance = MathUtils.random(4, 6);
        int bonusDamage = MathUtils.random(2, Math.max(level / 4, 2));
        int manaCost = (bonusDamage + 1);

        return new IceGrimoire(level, manaCost, minDistance, bonusDamage);
    }

    public static LightningGrimoire generateLightningGrimoire(int level) {
        int minDistance = MathUtils.random(4, 6);
        int bonusDamage = MathUtils.random(2, Math.max(level / 2, 2));
        int manaCost = (bonusDamage + 1);

        return new LightningGrimoire(level, manaCost, minDistance, bonusDamage);
    }

    public static KindOfWeapon generateRandomGrimoire(int level) {
        switch (MathUtils.random(3)) {
            case 0:
                return generateIceGrimoire(level);
            case 1:
                return generatePoisonGrimoire(level);
            case 2:
                return generateLightningGrimoire(level);
            case 3:
            default:
                return generateFireGrimoire(level);
        }
    }
}
