/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.ItemCategory;

public class Sword extends MeleeWeapon implements Weapon {

    public Sword(int bonusStr, int bonusDex, int bonusCon, int bonusWis, int bonusDamage) {
        super("Sword", "A simply sword", bonusStr, bonusDex, bonusCon, bonusWis, bonusDamage);
        this.description = getRandomDescription();
    }

    @Override
    public Sword copy() {
        return new Sword(getBonusStr(), getBonusDex(), getBonusCon(), getBonusWis(), getBonusDamage());
    }

    @Override
    public void attack(Character attacker, Character target, Level level) {
        if (MathUtils.random(1, 20) + attacker.getDex() >
                MathUtils.random(1, 20) + target.getDex()) {
            int dmg = attacker.getStr() + rollBonusDamage();
            GameEvent.attackHit(attacker, target, dmg, getCategory());
            target.receiveHit(dmg, attacker);
        } else {
            GameEvent.attackMissed(attacker, target);
        }
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.WEAPON_SWORD;
    }

    private String getRandomDescription() {
        String[] desc = {"A very long, wide, jagged blade made of ceramic is held by a grip wrapped in extremely rare, dark brown pig leather.",
                "With just a razor-sharp point this weapon will make sure your enemies are full of holes with deadly speed and precision.",
                "The blade has a thin, curved cross-guard, offering just enough protection to hands, as well as adding a weight balance to the blade.",
                "The cross-guard has an ornamented ring on each side, marking the house it belongs to.",
                "A fairly large pommel is decorated with gilded linings, no expense is spared for this gorgeous weapon.",
                "A fairly short, thick, slightly curved blade made of obsidian is held by a grip wrapped in fairly common, maroon cow leather.",
                "Dual-edged and razor-sharp this weapon is the champion's choice. It'll crush your enemies with cleaving hacks and piercing stabs.",
                "The blade has a spiked cross-guard, adding just enough weight to make sure the blade sits firmly in the owner's hand.",
                "The cross-guard has a simple cross on each side, which shows how ordinary this weapon is.",
                "A large pommel is marked with a quality symbol, a decent symbol for a decent weapon.",
                "A fairly short, thin, straight blade made of copper is held by a grip wrapped in extravagant, onyx bear leather.",
                "A single, sharp edge makes sure this weapon is not just for hacking and slashing, but also great to block incoming attacks.",
                "The blade has a narrow, twisted cross-guard, offering plenty of protection to the owner's hands and thus his or her life.",
                "The cross-guard has a lavish fish scale on each side, this weapon was clearly a custom order, probably by an important figure.",
                "A wide pommel is decorated with gilded linings, fine details which prove how carefully this weapon was crafted.",
                "A fairly long, wide, smooth blade made of crystal is held by a grip wrapped in extremely rare, deep orange snake leather.",
                "The sharp, dual-edged blade makes this weapon the best choice for both cleaving and stabbing your enemies with ferocious power.",
                "The blade has a spiked, twisted cross-guard, offering plenty of protection to the owner's hands and thus his or her life.",
                "The cross-guard has a plain loop on each side, which shows how ordinary this weapon is.",
                "A fairly small pommel is decorated with common gems, anything better would be a waste.",
                "A fairly short, thin, jagged blade made of ivory is held by a grip wrapped in rare, light brown bear leather.",
                "A sharp, dual-edged blade makes this weapon the choice for any warrior looking for a versatile weapon ideal for any combat style.",
                "The blade has a curved cross-guard, which makes sure the blade is both balanced and capable of protecting against any sliding sword.",
                "The cross-guard has a simple loop on each side, this weapon clearly isn't a one of a kind.",
                "A fairly small pommel is decorated with common gems, nothing too impressive, but that's to be expected.",
                "A large, wide, warped blade made of iron is held by a grip wrapped in regular, maroon cow leather.",
                "Because it only has a razor-sharp point this weapon will make sure your enemies are full of holes with deadly speed and precision.",
                "The blade has straight cross-guard, adding weight to the blade for a better balance, as well as offering protection during battle.",
                "The cross-guard has a basic orb on each side, so this weapon is for just about anybody with the right amount of cash.",
                "A wide pommel is engraved with the symbol of the house this sword belongs to, a symbol which doesn't prove much.",
                "A fairly short, slim, warped blade made of ceramic is held by a grip wrapped in strange, red salmon leather.",
                "A fine, sharp point makes this weapon ideal to pierce your enemies and turn them into a sieve.",
                "The blade has a jagged, twisted cross-guard, creating the ideal weight balance to allow for smooth and accurate swings with this blade.",
                "The cross-guard has a basic coil on each side, which is common on many weapons.",
                "A wide pommel is decorated with common gems, but the weapon is supposed to cut after all, not look fancy.",
                "A fairly large, wide, warped blade made of steel is held by a grip wrapped in dull, gold colored ray skin.",
                "Dual-edged and razor-sharp this weapon is the champion's choice.",
                "It'll crush your enemies with cleaving hacks and piercing stabs.",
                "The blade has a thin, curled cross-guard, large enough to make sure your fingers are safe and the blade will remain firmly in your hands.",
                "The cross-guard has an elaborate lion paw on each side, this is clearly a weapon not meant to be wielded by a commoner.",
                "A large pommel is marked with a quality symbol, a symbol many are jealous of.",
                "A fairly large, slim, jagged blade made of steel is held by a grip wrapped in extravagant, bronze goat leather.",
                "Because it only has a razor-sharp point this weapon is the ideal choice to turn your enemies into Swiss cheese.",
                "The blade has a large, straight cross-guard, offering plenty of protection to the owner's hands and thus his or her life.",
                "The cross-guard has a gilded hawk beak on each side, this weapon is clearly meant to be taken care of with dedication.",
                "A large pommel is decorated with precious gems, no expense is spared for this gorgeous weapon.",
                "A large, narrow, smooth blade made of copper is held by a grip wrapped in common, forest green ray skin.",
                "With its sharp, dual-edged blade this weapon will slice, dice, stab and jab your enemies and shred whatever's left of them.",
                "The cross-guard has a jeweled horn on each side, a unique design for a unique weapon.",
                "A fairly large pommel is marked with a quality symbol, a symbol of true greatness."};
        return desc[MathUtils.random(desc.length - 1)];
    }
}
