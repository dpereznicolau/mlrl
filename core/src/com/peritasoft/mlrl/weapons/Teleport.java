/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.weapons;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.LevelUtils;
import com.peritasoft.mlrl.dungeongen.Position;

public class Teleport {
    public void cast(Character caster, Level level) {
        Position pos = LevelUtils.findValidPosition(level);
        while (level.hasCharacterIn(pos)) {
            pos = LevelUtils.findValidPosition(level);
        }
        caster.setPosition(pos, level);
    }

    public void cast(Character caster, Level level, Character enemy) {
        Position pos;
        for (Direction dir: Direction.randomizedDirections()) {
            pos = new Position(enemy.getPositionX() + dir.x, enemy.getPositionY() + dir.y);
            if (level.isGround(pos) && !level.hasCharacterIn(pos)) {
                caster.setPosition(pos, level);
                return;
            }
        }
    }
}
