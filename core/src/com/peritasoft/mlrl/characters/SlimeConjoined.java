/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.WanderSeekApproach;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;

public class SlimeConjoined extends Character {
    public SlimeConjoined(int level, Position pos, Inventory inventory) {
        super(Demography.SLIME_CONJOINED, level, 0, 0, 0, 0, 3, pos.getX(), pos.getY(), new WanderSeekApproach(), inventory);
        this.setStr(level / 2);
        this.setDex((level / 3) + 1);
        this.setWis(1);
        this.setCon(level * 2);
        this.resetHp();
    }

    @Override
    public Action update(Level level) {
        for (Direction dir : Direction.randomizedDirections()) {
            final Cell nextCell = level.getCell(getPositionX() + dir.x, getPositionY() + dir.y);
            if (nextCell.isWalkable() && !nextCell.hasCharacter()) {
                level.addEnemy(new SlimeSmall(getLevel() / 2, new Position(getPosition(), dir)));
                replaceWithSmallSlime(level);
                return Action.MOVE;
            }
        }
        return super.update(level);
    }

    @Override
    public boolean onKilled(Level level, Character killer) {
        replaceWithSmallSlime(level);
        return false;
    }

    protected void replaceWithSmallSlime(Level level) {
        level.removeEnemy(this);
        level.addEnemy(new SlimeSmall(getLevel() / 2, getPosition(), getInventory()));
    }

}
