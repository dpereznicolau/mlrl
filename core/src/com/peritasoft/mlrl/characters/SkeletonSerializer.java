/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;

public abstract class SkeletonSerializer<T extends Skeleton> extends CharacterSerializer<T> {
    @Override
    protected void writeOwnProperties(Json json, T object) {
        json.writeValue(object.doesGiveXP());
        json.writeValue(object.raise);
    }

    @Override
    protected T createInstance(Json json, JsonValue.JsonIterator iter, int level, Position position, Inventory inventory) {
        final boolean givesXp = iter.next().asBoolean();
        final boolean raise = iter.next().asBoolean();
        T object = createInstance(level, position, givesXp, inventory);
        object.raise = raise;
        return object;
    }

    protected abstract T createInstance(int level, Position position, boolean givesXp, Inventory inventory);
}
