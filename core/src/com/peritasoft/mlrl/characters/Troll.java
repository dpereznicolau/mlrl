/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.WanderSeekApproach;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;

public class Troll extends Character {

    private int hpRegenTurn;

    public Troll(int level, Position pos) {
        this(level, pos, new Inventory());
    }

    public Troll(int level, Position pos, Inventory inventory) {
        super(Demography.TROLL, level, 0, 0, 0, 0, 5, pos.getX(), pos.getY(),
                new WanderSeekApproach(false),
                inventory);
        this.setStr(level * 2);
        this.setDex((level - (level / 4)));
        this.setWis(2);
        this.setCon(level * 3);
        this.resetHp();
        this.hpRegenTurn = 0;
    }

    @Override
    public Action update(Level level) {
        if (canRegenHp()) {
            hpRegen();
        } else {
            hpRegenTurn += 1;
            if (hpRegenTurn == 5) {
                hpRegenTurn = 0;
                setRegenHp(true);
            }
        }
        return super.update(level);
    }

    protected void hpRegen() {
        if (getHp() < getMaxHp()) {
            hpRegenTurn += 1;
            if (hpRegenTurn == 5) {
                hpRegenTurn = 0;
                int hpHealed = MathUtils.random(5, 15);
                heal(hpHealed);
            }
        }
    }
}

