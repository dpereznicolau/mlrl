/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.EkimusBehaviour;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.weapons.Shootable;
import com.peritasoft.mlrl.weapons.VoidBall;

public class Ekimus extends Boss {
    private VoidBall voidAtk;

    public Ekimus(int level, Position pos) {
        this(level, pos, new Inventory());
    }

    public Ekimus(int level, Position pos, Inventory inventory) {
        super(Demography.EKIMUS, level, 0, 0, 0, 0, 7, pos.getX(), pos.getY(),
                new EkimusBehaviour(), inventory);
        this.setStr(level * 2);
        this.setDex(level);
        this.setWis(level * 2);
        this.setCon(level * 5);
        //weapons
        voidAtk = new VoidBall(MathUtils.random(10, 15), (this.getMaxMp() - 1) / 2, 1);
        this.resetHp();
        this.fullMp();
    }

    @Override
    public Shootable shootWeapon() {
        return voidAtk;
    }

    @Override
    public void receiveHit(int damage, Character enemy) {
        if (getDemography() == Demography.MIST) {
            GameEvent.intangibleTarget(this);
            return;
        }
        super.receiveHit(damage, enemy);
    }

    public void setDemography(Demography demography) {
        this.demography = demography;
    }
}
