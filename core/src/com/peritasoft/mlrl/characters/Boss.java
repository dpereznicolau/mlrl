/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.Behaviour;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.KindOfWeapon;
import com.peritasoft.mlrl.props.Chest;
import com.peritasoft.mlrl.props.Heart;
import com.peritasoft.mlrl.props.Prop;
import com.peritasoft.mlrl.weapons.WeaponGenerator;

public abstract class Boss extends Character {

    Boss(Demography demography, int level, int str, int dex, int wis, int con,
         int sightRadius, int positionX, int positionY, Behaviour ai, Inventory inventory) {
        super(demography, level, str, dex, wis, con, sightRadius, positionX, positionY, ai, inventory);
    }

    @Override
    public boolean onKilled(Level level, Character killer) {
        level.putStaircaseDown(getPosition());
        KindOfWeapon item;
        switch (killer.getDemography()) {
            case HERO_WARRIOR:
                item = WeaponGenerator.generateSword(getLevel());
                break;
            case HERO_ARCHER:
                item = WeaponGenerator.generateBow(getLevel());
                break;
            case HERO_MAGE:
                item = WeaponGenerator.generateRandomGrimoire(getLevel());
                break;
            default:
                item = WeaponGenerator.generateDagger(getLevel());
        }
        final Chest chest = new Chest(item);
        placePropAround(level, killer, chest);
        getInventory().empty();
        placePropAround(level, killer, new Heart());
        return true;
    }

    protected void placePropAround(Level level, Character character, Prop prop) {
        final int radius = 3;
        final int minY = character.getPositionY() - radius;
        final int maxY = character.getPositionY() + radius;
        final int minX = character.getPositionX() - radius;
        final int maxX = character.getPositionX() + radius;
        for (int y = minY; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                final Cell cell = level.getCell(x, y);
                if (!cell.isWalkable() || cell.hasProp()) {
                    continue;
                }
                cell.setProp(prop);
                return;
            }
        }
    }

    @Override
    public int getXp(float playerLevel) {
        return super.getXp(playerLevel) * 4;
    }
}
