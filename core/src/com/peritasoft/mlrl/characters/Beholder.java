/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.Runaway;
import com.peritasoft.mlrl.ai.StayAtRangeAndShoot;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.weapons.Shootable;
import com.peritasoft.mlrl.weapons.VoidBall;

public class Beholder extends Character {
    private VoidBall voidAtk;

    public Beholder(int level, Position pos) {
        this(level, pos, new Inventory());
    }

    public Beholder(int level, Position pos, Inventory inventory) {
        super(Demography.BEHOLDER, level, 0, 0, 0, 0, 6, pos.getX(), pos.getY(),
                new StayAtRangeAndShoot(new Runaway(), 4), inventory);
        this.setStr(level);
        this.setDex(level - 5);
        this.setWis((int) (level * 1.75f));
        this.setCon(level);
        //weapons
        voidAtk = new VoidBall(level / 3, level / 4, 0.5f);
        this.resetHp();
        this.fullMp();
    }

    @Override
    public Shootable shootWeapon() {
        return voidAtk;
    }

}
