/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Mimic;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.item.ItemGenerator;
import com.peritasoft.mlrl.item.KindOfWeapon;
import com.peritasoft.mlrl.item.Potion;
import com.peritasoft.mlrl.props.Chest;
import com.peritasoft.mlrl.props.ChestEmpty;
import com.peritasoft.mlrl.props.MimicDecoy;
import com.peritasoft.mlrl.weapons.WeaponGenerator;

public class ChestPutter implements LevelGenerator {
    private final LevelGenerator next;

    public ChestPutter(LevelGenerator next) {
        if (next == null) {
            throw new IllegalArgumentException("next generator can not be null");
        }
        this.next = next;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        Level level = next.generate(floor, upStaircase, player);
        int numChests = MathUtils.random(1, 3);
        for (int i = 0; i < numChests; i++) {
            Position pos = LevelUtils.findValidPosition(level);
            Cell cell = level.getCell(pos);
            if (!cell.hasProp() && !cell.hasCharacter()) {
                if (i == 0 && (floor % 10 == 3 || floor % 10 == 7)) {
                    float rand = MathUtils.random();
                    KindOfWeapon item;
                    switch (player.getDemography()) {
                        case HERO_WARRIOR:
                            if (rand < 0.65) {
                                item = WeaponGenerator.generateSword(floor + 1);
                            } else if (rand < 0.80) {
                                item = WeaponGenerator.generateBow(floor + 1);
                            } else if (rand < 0.95) {
                                item = WeaponGenerator.generateDagger(floor + 1);
                            } else {
                                item = WeaponGenerator.generateFireSpellgem(floor + 1);
                            }
                            break;
                        case HERO_ARCHER:
                            if (rand < 0.65) {
                                item = WeaponGenerator.generateBow(floor + 1);
                            } else if (rand < 0.80) {
                                item = WeaponGenerator.generateSword(floor + 1);
                            } else if (rand < 0.95) {
                                item = WeaponGenerator.generateDagger(floor + 1);
                            } else {
                                item = WeaponGenerator.generateFireSpellgem(floor + 1);
                            }
                            break;
                        case HERO_MAGE:
                            if (rand < 0.65) {
                                item = WeaponGenerator.generateRandomGrimoire(floor + 1);
                            } else if (rand < 0.80) {
                                item = WeaponGenerator.generateSword(floor + 1);
                            } else if (rand < 0.95) {
                                item = WeaponGenerator.generateDagger(floor + 1);
                            } else {
                                item = WeaponGenerator.generateBow(floor + 1);
                            }
                            break;
                        default:
                            if (rand < 0.25) {
                                item = WeaponGenerator.generateBow(floor + 1);
                            } else if (rand < 0.50) {
                                item = WeaponGenerator.generateSword(floor + 1);
                            } else if (rand < 0.75) {
                                item = WeaponGenerator.generateDagger(floor + 1);
                            } else {
                                item = WeaponGenerator.generateFireSpellgem(floor + 1);
                            }
                    }
                    cell.setProp(new Chest(item));
                } else {
                    float rand = MathUtils.random();
                    if (rand < 0.85) {
                        Item treasure = MathUtils.randomBoolean() ? new Potion() : ItemGenerator.generateScroll(floor);
                        Chest chest = new Chest(treasure);
                        cell.setProp(chest);
                    } else if (rand < 0.95) {
                        cell.setProp(new MimicDecoy(new Mimic(floor + 1, pos)));
                    } else {
                        cell.setProp(new ChestEmpty());
                    }
                }
            }
        }
        return level;
    }

    public LevelGenerator getNext() {
        return next;
    }

}
