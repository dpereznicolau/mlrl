/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.PathFinder;
import com.peritasoft.mlrl.characters.PlayerHero;

import java.util.Arrays;
import java.util.Queue;

public class MazeLevelGenerator implements LevelGenerator {
    private Tile[][] tiles;
    private LevelUtils.Tilemap tileMap;
    private final int width;
    private final int height;
    private Position start;
    private Position goal;
    private int distanceToGoal;

    public MazeLevelGenerator(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        //Room width/Heith is constant
        tiles = new Tile[width][height];
        tileMap = new LevelUtils.Tilemap(tiles);
        fillWithWalls(tiles);
        carveMaze(tiles);
        return new Level(tiles, start, LevelType.DUNGEON);
    }

    private void fillWithWalls(Tile[][] tiles) {
        for (Tile[] row : tiles) {
            Arrays.fill(row, Tile.WALL);
        }
    }

    private void carveMaze(Tile[][] tiles) {
        start = startingPosition(tiles);
        goal = start;
        distanceToGoal = 0;
        carve(tiles, start);
        tiles[goal.getY()][goal.getX()] = Tile.STAIRCASEDOWN;
    }

    private void carve(Tile[][] tiles, Position pos) {
        Direction[] randomDirs = Direction.randomizedDirections();
        for (Direction randomDir : randomDirs) {
            switch (randomDir) {
                case NORTH:
                    if (pos.getY() + 2 >= height - 1)
                        continue;
                    if (tiles[pos.getY() + 2][pos.getX()] != Tile.GROUND
                            && tiles[pos.getY() + 2][pos.getX()] != Tile.STAIRCASEUP) {
                        tiles[pos.getY() + 2][pos.getX()] = Tile.GROUND;
                        tiles[pos.getY() + 1][pos.getX()] = Tile.GROUND;
                        carve(tiles, new Position(pos.getX(), pos.getY() + 2));
                    }
                    break;
                case EAST:
                    if (pos.getX() + 2 >= width - 1)
                        continue;
                    if (tiles[pos.getY()][pos.getX() + 2] != Tile.GROUND
                            && tiles[pos.getY()][pos.getX() + 2] != Tile.STAIRCASEUP) {
                        tiles[pos.getY()][pos.getX() + 2] = Tile.GROUND;
                        tiles[pos.getY()][pos.getX() + 1] = Tile.GROUND;
                        carve(tiles, new Position(pos.getX() + 2, pos.getY()));
                    }
                    break;
                case SOUTH:
                    if (pos.getY() - 2 <= 0)
                        continue;
                    if (tiles[pos.getY() - 2][pos.getX()] != Tile.GROUND
                            && tiles[pos.getY() - 2][pos.getX()] != Tile.STAIRCASEUP) {
                        tiles[pos.getY() - 2][pos.getX()] = Tile.GROUND;
                        tiles[pos.getY() - 1][pos.getX()] = Tile.GROUND;
                        carve(tiles, new Position(pos.getX(), pos.getY() - 2));
                    }
                    break;
                case WEST:
                    if (pos.getX() - 2 <= 0)
                        continue;
                    if (tiles[pos.getY()][pos.getX() - 2] != Tile.GROUND
                            && tiles[pos.getY()][pos.getX() - 2] != Tile.STAIRCASEUP) {
                        tiles[pos.getY()][pos.getX() - 2] = Tile.GROUND;
                        tiles[pos.getY()][pos.getX() - 1] = Tile.GROUND;
                        carve(tiles, new Position(pos.getX() - 2, pos.getY()));
                    }
                    break;
            }
        }
        Queue<Position> path = PathFinder.find(tileMap,start,pos);
        if (path != null) {
            int distanceToPos = path.size();
            if (distanceToPos > distanceToGoal && isEndTile(tiles, pos)) {
                goal = pos;
                distanceToGoal = distanceToPos;
            }
        }
    }

    private Position startingPosition(Tile[][] tiles) {
        //Cerco posicio incial imparella i que no estigui a cap cantonada
        int y = MathUtils.random(width - 2);
        while (y % 2 == 0) {
            y = MathUtils.random(width - 2);
        }

        int x = MathUtils.random(height - 2);
        while (x % 2 == 0) {
            x = MathUtils.random(height - 2);
        }

        tiles[y][x] = Tile.STAIRCASEUP;
        return new Position(x,y);
    }

    private boolean isEndTile(Tile[][] tiles, Position pos) {
        //qualsevol casella amb exactament 3 murs al voltant es un fi de cami
        int numWalls = 0;
        for (Direction dir : Direction.values()) {
            if (tiles[pos.getY() + dir.y][pos.getX() + dir.x] == Tile.WALL) numWalls++;
        }
        return numWalls == 3;
    }

}
