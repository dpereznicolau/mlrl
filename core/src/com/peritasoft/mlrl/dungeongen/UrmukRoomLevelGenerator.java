/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.Troll;
import com.peritasoft.mlrl.characters.Urmuk;
import com.peritasoft.mlrl.props.*;

import java.util.Arrays;

public class UrmukRoomLevelGenerator implements LevelGenerator {
    private static final int ROOM_WIDTH = 13;
    private static final int ROOM_HEIGHT = 10;

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        //Room width/Heith is constant
        Tile[][] tiles = new Tile[ROOM_HEIGHT][ROOM_WIDTH];
        fillWithGround(tiles);
        putWalls(tiles);
        Position startingPosition = new Position(ROOM_WIDTH / 2, 2);
        String[] logMessages = new String[]{
                "A powerful ork stands behind two enormous stinky Trolls",
                "Urmuk KILL!! Urmuk CRUSH!!"
        };
        final Level level = new BossLevel(tiles, startingPosition, LevelType.DUNGEON, logMessages);
        decorateRoom(level);
        generateBoss(level, floor, player.getLevel());
        return level;
    }

    private void fillWithGround(Tile[][] tiles) {
        for (Tile[] row : tiles) {
            Arrays.fill(row, Tile.GROUND);
        }
    }

    private void putWalls(Tile[][] tiles) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[0].length; j++) {
                if (i == 0 || i == tiles.length - 1 || j == 0 || j == tiles[0].length - 1) {
                    tiles[i][j] = Tile.WALL;
                }
            }
        }
    }

    private void decorateRoom(Level level) {
        setVariations(level);
        level.getCell(ROOM_WIDTH / 2 - 2, 6).setProp(new Statue(PropType.STATUE_WARRIOR));
        level.getCell(ROOM_WIDTH / 2 - 2, 3).setProp(new Statue(PropType.STATUE_WARRIOR));
        level.getCell(ROOM_WIDTH / 2 + 2, 6).setProp(new Statue(PropType.STATUE_WARRIOR));
        level.getCell(ROOM_WIDTH / 2 + 2, 3).setProp(new Statue(PropType.STATUE_WARRIOR));
        level.getCell(ROOM_WIDTH / 2, 8).setProp(new Throne());
        level.getCell(2, 9).setProp(new Torch());
        level.getCell(ROOM_WIDTH / 2, 9).setProp(new Torch());
        level.getCell(ROOM_WIDTH - 3, 9).setProp(new Torch());
        level.getCell(2, 1).setProp(new LightPoint());
        level.getCell(ROOM_WIDTH / 2, 1).setProp(new LightPoint());
        level.getCell(ROOM_WIDTH - 3, 1).setProp(new LightPoint());
        level.getCell(0, 3).setProp(new LightPoint());
        level.getCell(0, 6).setProp(new LightPoint());
        level.getCell(ROOM_WIDTH - 1, 3).setProp(new LightPoint());
        level.getCell(ROOM_WIDTH - 1, 6).setProp(new LightPoint());
    }

    private void setVariations(Level level) {
        for (int i = 0; i < ROOM_WIDTH; i++) {
            for (int j = 0; j < ROOM_HEIGHT; j++) {
                if (i == 0 || i == ROOM_WIDTH - 1 || j == 0 || j == ROOM_HEIGHT - 1) {
                    if (level.getCell(i, j).variation == 4 || level.getCell(i, j).variation == 5) {
                        level.getCell(i, j).variation = 1;
                    }
                }
            }
        }
        level.getCell(ROOM_WIDTH / 2 - 2, 9).variation = 4;
        level.getCell(ROOM_WIDTH / 2 + 2, 9).variation = 4;
        level.getCell(ROOM_WIDTH - 1, 6).variation = 5;
        level.getCell(0, 6).variation = 5;
        level.getCell(ROOM_WIDTH - 1, 3).variation = 5;
        level.getCell(0, 3).variation = 5;
        level.getCell(ROOM_WIDTH / 2, 8).variation = 6;
        level.getCell(ROOM_WIDTH / 2, 7).variation = 7;
        level.getCell(ROOM_WIDTH / 2, 6).variation = 9;
        level.getCell(ROOM_WIDTH / 2, 5).variation = 6;
        level.getCell(ROOM_WIDTH / 2, 4).variation = 8;
        level.getCell(ROOM_WIDTH / 2, 3).variation = 6;
        level.getCell(ROOM_WIDTH / 2, 2).variation = 10;
    }

    private void generateBoss(Level level, int floor, int playerLevel) {
        int enemyLevel = Math.max(floor, playerLevel);
        createTroll(enemyLevel, level, new Position(ROOM_WIDTH / 2 - 2, 8));
        createTroll(enemyLevel, level, new Position(ROOM_WIDTH / 2 + 2, 8));
        final Urmuk urmuk = new Urmuk(enemyLevel + 3, new Position(ROOM_WIDTH / 2, 8));
        level.addEnemy(urmuk);
    }

    private Troll createTroll(int enemyLevel, Level level, Position pos) {
        Troll troll = new Troll(enemyLevel + 2, pos);
        level.addEnemy(troll);
        return troll;
    }


}
