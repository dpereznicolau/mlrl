/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.utils.Array;
import com.peritasoft.mlrl.characters.PlayerHero;

public class MemoizedLevelGenerator implements LevelGenerator {
    private final LevelGenerator next;
    final Array<Level> levels;
    private final int floorWidth;
    private final int floorHeight;
    private final boolean isEndless;

    public MemoizedLevelGenerator(LevelGenerator next, int floorWidth, int floorHeight, boolean isEndless) {
        this.floorWidth = floorWidth;
        this.floorHeight = floorHeight;
        this.isEndless = isEndless;
        if (next == null) {
            throw new IllegalArgumentException("next level generator can not be null");
        }
        this.next = next;
        levels = new Array<>();
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        levels.setSize(Math.max(floor + 1, levels.size));
        Level level = levels.get(floor);
        if (level == null) {
            level = next.generate(floor, upStaircase, player);
            levels.set(floor, level);
        }
        return level;
    }

    public LevelGenerator getNext() {
        return next;
    }

    public int getFloorWidth() {
        return floorWidth;
    }

    public int getFloorHeight() {
        return floorHeight;
    }

    public boolean isEndless() {
        return isEndless;
    }
}
