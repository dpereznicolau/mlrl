/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.PlayerHeroSerializer;
import com.peritasoft.mlrl.item.PotionVariations;

public class DungeonSerializer implements Json.Serializer<Dungeon> {
    private final PlayerHeroSerializer playerHeroSerializer;

    public DungeonSerializer(PlayerHeroSerializer playerHeroSerializer) {
        this.playerHeroSerializer = playerHeroSerializer;
    }

    @Override
    public void write(Json json, Dungeon object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.getCurrentFloor());
        json.writeValue(object.nextTurn);
        json.writeValue(object.getPotionVar());
        json.writeValue(object.generator, MemoizedLevelGenerator.class);
        json.writeValue(object.attackingCharacter == null
                ? new Position(-1, -1)
                : object.attackingCharacter.getPosition()
        );
        json.writeArrayEnd();
    }

    @Override
    public Dungeon read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator iter = jsonData.iterator();
        final int currentFloor = iter.next().asInt();
        final PlayerHero playerHero = playerHeroSerializer.lastReadPlayer();
        final boolean nextTurn = iter.next().asBoolean();
        final PotionVariations potionVar = json.readValue(PotionVariations.class, iter.next());
        final LevelGenerator generator = json.readValue(MemoizedLevelGenerator.class, iter.next());
        final Position attackingCharacterPos = json.readValue(Position.class, iter.next());

        final Dungeon dungeon = new Dungeon(currentFloor + 1, generator, playerHero);
        dungeon.potionVar = potionVar;
        dungeon.generateFirstLevel(playerHero.getPosition());
        dungeon.nextTurn = nextTurn;
        dungeon.attackingCharacter = dungeon.level.getCell(attackingCharacterPos).getCharacter();

        generator.generate(currentFloor, currentFloor > 1, playerHero);
        return dungeon;
    }
}
