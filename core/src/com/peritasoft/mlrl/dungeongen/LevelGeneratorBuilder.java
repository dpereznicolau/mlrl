/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

public class LevelGeneratorBuilder {

    public static LevelGenerator buildLevelGenerator(int floorWidth, int floorHeight) {
        LevelGenerator[] generators = generators(floorWidth, floorHeight);
        MixedLevelGenerator.GeneratorFloor[] generatorFloors = {
                new MixedLevelGenerator.GeneratorFloor(5, generators[0]),
                new MixedLevelGenerator.GeneratorFloor(10, generators[1]),
                new MixedLevelGenerator.GeneratorFloor(11, generators[2]),
                new MixedLevelGenerator.GeneratorFloor(19, generators[3]),
                new MixedLevelGenerator.GeneratorFloor(20, generators[4]),
                new MixedLevelGenerator.GeneratorFloor(22, generators[5]),
                new MixedLevelGenerator.GeneratorFloor(26, generators[6]),
                new MixedLevelGenerator.GeneratorFloor(30, generators[7]),
                new MixedLevelGenerator.GeneratorFloor(31, generators[8]),
                new MixedLevelGenerator.GeneratorFloor(39, generators[9]),
                new MixedLevelGenerator.GeneratorFloor(40, generators[10]),
                new MixedLevelGenerator.GeneratorFloor(49, generators[11]),
                new MixedLevelGenerator.GeneratorFloor(50, generators[12]),
        };
        return new MixedLevelGenerator(generatorFloors);
    }

    private static LevelGenerator[] generators(int floorWidth, int floorHeight) {
        return new LevelGenerator[]{
                arrowAndChestPutters(new EnemyPutterLowerSewers(10, new CenterLevelGenerator(8, floorWidth, floorHeight, LevelType.SEWER))),
                arrowAndChestPutters(new EnemyPutterUpperSewers(10, new CenterLevelGenerator(8, floorWidth, floorHeight, LevelType.SEWER))),
                new TrasgaRoomLevelGenerator(),
                arrowAndChestPutters(new EnemyPutterGoblin(10, new RoomsLevelGenerator(floorWidth, floorHeight, LevelType.DUNGEON))),
                new UrmukRoomLevelGenerator(),
                new EnemyPutterMaze(5, new MazeLevelGenerator(floorWidth - 1, floorHeight - 1)),
                arrowAndChestPutters(new EnemyPutterLowerCrypt(10, new RoomsLevelGenerator(floorWidth, floorHeight, LevelType.CRYPT))),
                arrowAndChestPutters(new EnemyPutterUpperCrypt(10, new RoomsLevelGenerator(floorWidth, floorHeight, LevelType.CRYPT))),
                new EkimusRoomLevelGenerator(),
                arrowAndChestPutters(new EnemyPutterLowerCaves(10, new CaveLevelGenerator(floorWidth, floorHeight))),
                new IvrexaRoomLevelGenerator(),
                arrowAndChestPutters(new EnemyPutterUpperCaves(10, new CaveLevelGenerator(floorWidth, floorHeight))),
                new MutableRoomLevelGenerator(),
        };
    }

    private static LevelGenerator arrowAndChestPutters(LevelGenerator generator) {
        return new ArrowPutter(new ChestPutter(generator));
    }

    public static LevelGenerator buildEndlessLevelGenerator(int floorWidth, int floorHeight) {
        LevelGenerator[] generators = generators(floorWidth, floorHeight);
        RandomLevelGenerator ordinary = new RandomLevelGenerator(
                new LevelGenerator[]{
                        generators[0],
                        generators[1],
                        generators[3],
                        generators[5],
                        generators[6],
                        generators[7],
                        generators[9],
                        generators[11],
                }
        );
        RandomLevelGenerator bosses = new RandomLevelGenerator(
                new LevelGenerator[]{
                        generators[2],
                        generators[4],
                        generators[8],
                        generators[10],
                }
        );
        MixedLevelGenerator.GeneratorFloor[] generatorFloors = {
                new MixedLevelGenerator.GeneratorFloor(1, generators[0]),
                new MixedLevelGenerator.GeneratorFloor(2, new EndlessLevelGenerator(ordinary, bosses))
        };
        return new MixedLevelGenerator(generatorFloors);
    }
}
