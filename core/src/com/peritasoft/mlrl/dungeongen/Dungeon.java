/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.effects.*;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.events.GameEventAdapter;
import com.peritasoft.mlrl.item.ItemCategory;
import com.peritasoft.mlrl.item.PotionVariations;

import java.util.Iterator;

public class Dungeon extends GameEventAdapter implements Disposable {
    Level level;
    private int currentFloor;
    final LevelGenerator generator;
    private PlayerHero player;
    PotionVariations potionVar;
    boolean nextTurn;
    Character attackingCharacter;
    boolean endGame;
    float distorting;

    public Dungeon(int startingFloor, final LevelGenerator generator, PlayerHero player) {
        this.generator = generator;
        this.nextTurn = false;
        this.currentFloor = startingFloor - 1;
        this.player = player;
        endGame = false;
        GameEvent.register(this);
    }

    @Override
    public void newGame(long seed) {
        // Initializing potion variation must come before generating the
        // first level, because if the level has any Character with a
        // Potion in its Inventory it would crash; this mainly happens
        // when starting with a “deep enough” level (e.g., level 10 or below)
        // as rats, the enemies in the first level, do not carry anything.
        potionVar = new PotionVariations();
        generateFirstLevel(null);
    }

    void generateFirstLevel(Position startingPosition) {
        final Level nextLevel = generateNextLevel();
        changeFloor(nextLevel, startingPosition == null
                ? nextLevel.getStartingPosition()
                : startingPosition);
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public PotionVariations getPotionVar() {
        return potionVar;
    }

    public Projectile getProjectile() {
        return getCurrentLevel().getProjectile();
    }

    public void previousFloor() {
        if (currentFloor > 0) {
            currentFloor--;
            final Level nextLevel = generateNextLevel();
            changeFloor(nextLevel, nextLevel.getPosStaircaseDown());
        }
    }

    public void nextFloor() {
        currentFloor++;
        final Level nextLevel = generateNextLevel();
        changeFloor(nextLevel, nextLevel.getStartingPosition());
    }

    private void changeFloor(Level nextLevel, Position startingPosition) {
        getPlayer().changeFloor(level, nextLevel, startingPosition);
        level = nextLevel;
        GameEvent.changedFloor(currentFloor, getCurrentLevel());
        nextTurn = true;
    }

    private Level generateNextLevel() {
        return generator.generate(currentFloor, currentFloor > 0, player);
    }

    public Level getCurrentLevel() {
        return level;
    }

    public void update(float delta) {
        if (distorting > 0) {
            distorting -= delta;
        }
        Iterator<LifeObj> iterator = getCurrentLevel().getLifeObjs().iterator();
        while (iterator.hasNext()) {
            LifeObj l = iterator.next();
            l.update(nextTurn);
            if (!l.isAlive()) {
                iterator.remove();
            }
        }
        nextTurn = false;
        if (attackingCharacter != null) {
            attackingCharacter.update(delta);
            if (!attackingCharacter.isAttacking()) {
                Character killer = this.attackingCharacter;
                this.attackingCharacter = null;
                if (wasPlayerKilled(killer)) {
                    return;
                }
                updateEnemies();
            }
        } else if (getProjectile() != null) {
            shoot(delta);
        }
        getCurrentLevel().update(delta);
    }

    public void update(PlayerHero.Key key) {
        if (attackingCharacter != null || getProjectile() != null || getPlayer().isDead()) {
            return;
        }
        Action action = getPlayer().update(getCurrentLevel(), key);
        if (action == Action.NONE) {
            return;
        }
        if (action == Action.MOVE) {
            if (getCurrentLevel().canDescendToNextFloor(getPlayer().getPosition())) {
                nextFloor();
                return;
            } else if (getCurrentLevel().canAscendToPreviousFloor(getPlayer().getPosition())) {
                previousFloor();
                return;
            }
        }
        if (getPlayer().isAttacking()) {
            attackingCharacter = getPlayer();
        } else {
            updateEnemies();
        }
    }

    private void shoot(float delta) {
        Projectile projectile = getProjectile();
        projectile.update(delta);
        Cell cell = getCurrentLevel().getCell(projectile.getPosition());
        if (cell.hasCharacter() && projectile.getShooter().canAttack(cell.getCharacter())) {
            projectile.impact(cell.getCharacter(), getCurrentLevel());
            destroyProjectile();
            return;
        }
        if (projectile.getPosition().equals(projectile.getFinalPosition())) {
            projectile.miss(getCurrentLevel());
            destroyProjectile();
        }
    }

    private void destroyProjectile() {
        getCurrentLevel().getLifeObjs().add(new Impact(getCurrentLevel().getProjectile()));
        getCurrentLevel().setProjectile(null);
        updateEnemies();
    }

    public void updateEnemies() {
        Iterable<Character> enemies = getCurrentLevel().getEnemies();
        for (Character c : enemies) {
            if (c.isUpdated()) continue;
            c.setUpdated(true);
            if (c.isDead()) {
                kill(c, getPlayer());
            } else {
                c.update(getCurrentLevel());
                if (c.isAttacking()) {
                    attackingCharacter = c;
                    return;
                }
                if (getCurrentLevel().getProjectile() != null) return;
            }
        }
        for (Character c : getCurrentLevel().getEnemies()) {
            c.setUpdated(false);
        }
        nextTurn = true;
    }

    private boolean wasPlayerKilled(Character killer) {
        if (getPlayer().isDead()) {
            getPlayer().onKilled(level, killer);
            return true;
        }
        return false;
    }

    private void kill(Character c, Character killer) {
        if (c.onKilled(getCurrentLevel(), killer)) {
            GameEvent.killed(c);
            if (c != getPlayer()) {
                getPlayer().killed(c);
            }
            getCurrentLevel().removeEnemy(c);
        }
    }

    public PlayerHero getPlayer() {
        return player;
    }

    @Override
    public void healed(Character character, int hpHealed) {
        getCurrentLevel().getLifeObjs().add(new FloatingNumber(FloatingNumber.Type.HEAL, hpHealed, character.getPosition()));
    }

    @Override
    public void attackHit(Character attacker, Character victim, int damage, ItemCategory itemCategory) {
        getCurrentLevel().getLifeObjs().add(new Attack(victim.getPosition()));
        getCurrentLevel().getLifeObjs().add(new FloatingNumber(FloatingNumber.Type.DAMAGE, damage, victim.getPosition()));
    }

    @Override
    public void attackMissed(Character attacker, Character victim) {
        getCurrentLevel().getLifeObjs().add(new Miss(victim.getPosition()));
    }

    @Override
    public void levelUp(int newLevel, int strUp, int dexUp, int wisUp, int conUp) {
        final Level currentLevel = getCurrentLevel();
        if (currentLevel == null) return;
        currentLevel.add(new LevelUp(player.getPosition()));
    }

    @Override
    public void attackByPoison(Character attacker, Character victim, int damage) {
        getCurrentLevel().add(new Attack(victim.getPosition()));
        if (victim.isDead()) {
            if (wasPlayerKilled(attacker)) {
                return;
            }
            kill(victim, attacker);
        }
    }

    @Override
    public void endGame(PlayerHero player) {
        endGame = true;
    }

    public boolean isEndGame() {
        return  endGame;
    }

    @Override
    public void dispose() {
        GameEvent.unregister(this);
    }

    @Override
    public void deathChangedPersona() {
        distorting = 0.25f;
    }

    public boolean shouldDistort() {
        return distorting > 0f;
    }
}
