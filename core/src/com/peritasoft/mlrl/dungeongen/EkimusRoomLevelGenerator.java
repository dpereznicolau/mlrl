/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.peritasoft.mlrl.characters.Ekimus;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.effects.LifeObjLayer;
import com.peritasoft.mlrl.effects.LifeTimeObj;
import com.peritasoft.mlrl.effects.PlayerRests;
import com.peritasoft.mlrl.effects.Web;
import com.peritasoft.mlrl.props.Altar;
import com.peritasoft.mlrl.props.Candles;
import com.peritasoft.mlrl.props.PropType;

import java.util.Arrays;

public class EkimusRoomLevelGenerator implements LevelGenerator {
    private static final int ROOM_WIDTH = 15;
    private static final int ROOM_HEIGHT = 15;


    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        //Room width/Heith is constant
        Tile[][] tiles = new Tile[ROOM_HEIGHT][ROOM_WIDTH];
        fillWithGround(tiles);
        putWalls(tiles);
        Position startingPosition = new Position(4, 6);
        String[] logMessages = new String[]{
                "An imposing figure awaits in this dark room",
                "I'm Ekimus, the immortal! Flee, you peasant"
        };
        final Level level = new BossLevel(tiles, startingPosition, LevelType.CRYPT2, logMessages);
        decorateRoom(level);
        generateBoss(level, floor, player.getLevel());
        return level;
    }

    private void fillWithGround(Tile[][] tiles) {
        for (Tile[] row : tiles) {
            Arrays.fill(row, Tile.GROUND);
        }
    }

    private void putWalls(Tile[][] tiles) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[0].length; j++) {
                if (i == 0 || i == tiles.length - 1 || j == 0 || j == tiles[0].length - 1) {
                    tiles[i][j] = Tile.WALL;
                }
            }
        }

        tiles[1][8] = Tile.WALL;
        tiles[2][8] = Tile.WALL;
        tiles[3][8] = Tile.WALL;
        tiles[4][8] = Tile.WALL;
        tiles[4][7] = Tile.WALL;
        tiles[4][6] = Tile.WALL;
        tiles[4][9] = Tile.WALL;
        tiles[4][10] = Tile.WALL;
        tiles[4][11] = Tile.WALL;

        tiles[10][1] = Tile.WALL;
        tiles[10][2] = Tile.WALL;
        tiles[10][3] = Tile.WALL;
        tiles[10][4] = Tile.WALL;
        tiles[11][4] = Tile.WALL;
        tiles[12][4] = Tile.WALL;
    }

    private void decorateRoom(Level level) {
        level.getCell(1, 11).setProp(new Candles());
        level.getCell(7, 13).setProp(new Candles());
        level.getCell(6, 5).setProp(new Candles());
        level.getCell(11, 5).setProp(new Candles());
        level.getCell(3, 9).setProp(new Candles());
        level.getCell(5, 11).setProp(new Candles());
        level.getCell(13, 8).setProp(new Candles());
        level.getCell(13, 12).setProp(new Candles());
        level.getCell(1, 5).setProp(new Candles());
        level.getCell(7, 3).setProp(new Candles());
        level.getCell(9, 3).setProp(new Candles());
        level.getCell(13, 3).setProp(new Candles());
        level.getCell(8, 9).setProp(new Altar(PropType.ALTAR_LEFT));
        level.getCell(9, 9).setProp(new Altar(PropType.ALTAR_CENTER));
        level.getCell(10, 9).setProp(new Altar(PropType.ALTAR_RIGHT));
        level.getCell(8, 7).setProp(new Altar(PropType.ALTAR));
        level.getCell(10, 7).setProp(new Altar(PropType.ALTAR_BONES));

        level.add(new Web(new Position(1, 13), 0, LifeObjLayer.OVER));
        level.add(new Web(new Position(13, 13), 1, LifeObjLayer.OVER));
        level.add(new Web(new Position(1, 1), 2, LifeObjLayer.OVER));
        level.add(new Web(new Position(13, 1), 3, LifeObjLayer.OVER));
        level.add(new Web(new Position(7, 1), 3, LifeObjLayer.OVER));
        level.add(new Web(new Position(9, 1), 2, LifeObjLayer.OVER));
        level.add(new Web(new Position(4, 2), 4, LifeObjLayer.UNDER));

        level.add(new PlayerRests(new Position(10, 1), LifeTimeObj.INFINITE));
        level.add(new PlayerRests(new Position(10, 2), LifeTimeObj.INFINITE));
        level.add(new PlayerRests(new Position(11, 1), LifeTimeObj.INFINITE));
        level.add(new PlayerRests(new Position(11, 2), LifeTimeObj.INFINITE));
        level.add(new PlayerRests(new Position(12, 1), LifeTimeObj.INFINITE));
        level.add(new PlayerRests(new Position(12, 2), LifeTimeObj.INFINITE));
    }


    private void generateBoss(Level level, int floor, int playerLevel) {
        int enemyLevel = Math.max(floor, playerLevel);
        level.addEnemy(new Ekimus(enemyLevel, new Position(9, 10)));
    }

}
