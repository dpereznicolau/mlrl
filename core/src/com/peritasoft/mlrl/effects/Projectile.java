/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.effects;

import com.badlogic.gdx.math.Vector2;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.weapons.Shootable;

public class Projectile {
    static final float SPEED = 192;
    Position position;
    Position finalPosition;
    Vector2 velocity;
    float offsetX;
    float offsetY;
    Character shooter;
    ProjectileType projectileType;
    Shootable shootable;

    public Projectile(Position finalPosition, Character shooter, ProjectileType projectileType, Shootable shootable) {
        this.position = new Position(shooter.getPosition());
        this.finalPosition = finalPosition;
        velocity = new Vector2(finalPosition.getX() - position.getX(), finalPosition.getY() - position.getY());
        velocity.nor();
        offsetX = 0;
        offsetY = 0;
        this.shooter = shooter;
        this.projectileType = projectileType;
        this.shootable = shootable;
    }

    public void update(float delta) {
        offsetY += (delta * SPEED * velocity.y);
        offsetX += (delta * SPEED * velocity.x);
        if (offsetY > 8) {
            offsetY -= 16;
            position.setY(position.getY() + 1);
        } else if (offsetY < -8) {
            offsetY += 16;
            position.setY(position.getY() - 1);
        }
        if (offsetX > 8) {
            offsetX -= 16;
            position.setX(position.getX() + 1);
        } else if (offsetX < -8) {
            offsetX += 16;
            position.setX(position.getX() - 1);
        }
    }

    public Position getPosition() {
        return position;
    }

    public Position getFinalPosition() {
        return finalPosition;
    }

    public int getOffsetX() {
        return (int) offsetX;
    }

    public int getOffsetY() {
        return (int) offsetY;
    }

    public Character getShooter() {
        return shooter;
    }

    public float getAngle() {
        return velocity.angle();
    }

    public ProjectileType getProjectileType() {
        return projectileType;
    }

    public void impact(Character target, Level level) {
        shootable.impact(shooter, target, level);
    }

    public void miss(Level level) {
        shootable.miss(shooter, level, position);
    }
}
