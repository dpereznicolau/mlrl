/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.characters.PlayerHeroSerializer;
import com.peritasoft.mlrl.events.Log;

public class LogSerializer implements Json.Serializer<Log> {
    private final PlayerHeroSerializer playerHeroSerializer;

    public LogSerializer(PlayerHeroSerializer playerHeroSerializer) {
        this.playerHeroSerializer = playerHeroSerializer;
    }

    @Override
    public void write(Json json, Log object, Class knownType) {
        json.writeArrayStart();
        for (int l = object.countLines() - 1; l >= 0; l--) {
            final Log.Line line = object.getLine(l);
            json.writeArrayStart();
            json.writeValue(line.message);
            json.writeValue(line.repeated);
            json.writeValue(line.color);
            json.writeArrayEnd();
        }
        json.writeArrayEnd();
    }

    @Override
    public Log read(Json json, JsonValue jsonData, Class type) {
        final Log log = new Log(playerHeroSerializer.lastReadPlayer());
        for (JsonValue lineArray : jsonData) {
            final JsonValue.JsonIterator line = lineArray.iterator();
            final String message = line.next().asString();
            final int repeat = line.next().asInt();
            final Color color = json.readValue(Color.class, line.next());
            for (int i = 0; i < repeat; i++) {
                log.add(message, color);
            }
        }
        return log;
    }
}
