/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.item.PotionColour;
import com.peritasoft.mlrl.item.PotionType;
import com.peritasoft.mlrl.item.PotionVariations;

import java.util.HashMap;

public class PotionVariationsSerializer implements Json.Serializer<PotionVariations> {
    @Override
    public void write(Json json, PotionVariations object, Class knownType) {
        json.writeArrayStart();
        for (PotionColour colour : PotionColour.values()) {
            json.writeValue(PotionVariations.getPotionType(colour));
        }
        json.writeArrayEnd();
    }

    @Override
    public PotionVariations read(Json json, JsonValue jsonData, Class type) {
        final HashMap<PotionColour, PotionType> variations = new HashMap<>();
        final JsonValue.JsonIterator iter = jsonData.iterator();
        for (PotionColour colour : PotionColour.values()) {
            variations.put(colour, json.readValue(PotionType.class, iter.next()));
        }
        return new PotionVariations(variations);
    }
}
