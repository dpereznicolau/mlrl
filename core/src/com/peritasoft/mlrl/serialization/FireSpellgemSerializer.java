/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.weapons.FireSpellgem;

public class FireSpellgemSerializer implements Json.Serializer<FireSpellgem> {
    @Override
    public void write(Json json, FireSpellgem object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.getUses());
        json.writeValue(object.getBaseDamage());
        json.writeValue(object.getBonusDamage());
        json.writeValue(object.getImpactChance());
        json.writeArrayEnd();
    }

    @Override
    public FireSpellgem read(Json json, JsonValue jsonData, Class type) {
        final JsonValue.JsonIterator stat = jsonData.iterator();
        final int uses = stat.next().asInt();
        final int baseDamage = stat.next().asInt();
        final int bonusDamage = stat.next().asInt();
        final float impactChange = stat.next().asFloat();
        return new FireSpellgem(uses, baseDamage, bonusDamage, impactChange);
    }

}
