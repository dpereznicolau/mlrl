/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class FixedSizeButton {
    private final Rectangle rect;
    private final TextureRegion texture;
    private final float rotation;

    public FixedSizeButton(float x, float y, TextureRegion texture) {
        this(x, y, texture, 0);
    }

    public FixedSizeButton(float x, float y, TextureRegion texture, float rotation) {
        rect = new Rectangle(x, y, 16f, 16f);
        this.texture = texture;
        this.rotation = rotation;
    }

    public void draw(Batch batch) {
        batch.draw(texture, rect.x, rect.y, rect.width / 2, rect.height / 2, rect.width, rect.height, 1, 1, rotation);
    }

    public boolean pressed(Vector2 position) {
        return rect.contains(position);
    }
}
