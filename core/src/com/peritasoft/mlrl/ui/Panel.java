/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Panel implements Widget {
    private static final int PADDING = 16;

    private final Rectangle rect;
    private final TextureRegion texture;

    public Panel(int x, int y, TextureAtlas.AtlasRegion texture) {
        final int width = texture.getRegionWidth() * 2;
        final int height = texture.getRegionHeight() * 2;
        this.rect = new Rectangle(x - width / 2f + PADDING, y - height / 2f + PADDING, width - PADDING * 2f, height - PADDING * 2f);
        this.texture = texture;
    }

    public void draw(Batch batch) {
        batch.draw(texture, rect.x - PADDING, rect.y - PADDING, rect.width + PADDING * 2f, rect.height + PADDING * 2f);
    }

    public boolean pressed(Vector2 position) {
        return rect.contains(position);
    }

    @Override
    public Rectangle getRect() {
        return rect;
    }
}
