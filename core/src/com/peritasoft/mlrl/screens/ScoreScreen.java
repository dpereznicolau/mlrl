/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.easing.EaseInBack;
import com.peritasoft.mlrl.easing.EaseOutBack;
import com.peritasoft.mlrl.easing.Easing;
import com.peritasoft.mlrl.render.*;
import com.peritasoft.mlrl.scores.Score;
import com.peritasoft.mlrl.ui.FixedSizeButton;

import java.util.List;

public class ScoreScreen extends MlrlScreenAdapter implements GestureDetector.GestureListener {

    private final int PADDING = 10;
    int RECORD_WIDTH = 339;
    int RECORD_HEIGHT = 40;
    int RECORD_SPACING = 3;

    private final TextureAtlas atlasInterface;
    private final NinePatch background;
    private final MyLittleRogueLike game;
    private final NinePatch listBackground;
    private final PortraitMap portraits;
    private final FixedSizeButton buttonClose;
    private final BitmapFont textFont;
    private final RendererAtlas atlas;
    private final NinePatch scroll;
    private final TextureRegion scrollHandle;
    private TitleBackground titleBackground;
    private float timer;
    private final List<Score> scores;
    private final Rectangle scissors;
    private final Rectangle listRect;
    private float scrollY;
    private final float maxScrollY;
    private final GestureDetector gestureDetector;
    private final Vector2 tapPoint;

    private Easing easing;
    private final Matrix4 transformation;
    private Runnable fadingTo;

    public ScoreScreen(MyLittleRogueLike game, TitleBackground background) {
        this.game = game;
        this.titleBackground = background;
        this.textFont = game.getSmallFont();
        atlasInterface = new TextureAtlas("td_interface.atlas");
        this.background = new NinePatch(atlasInterface.findRegion("backgroundInventory"), 16, 16, 16, 16);
        listBackground = new NinePatch(atlasInterface.findRegion("backgroundList"), 10, 10, 5, 5);
        portraits = new PortraitMap();
        buttonClose = new FixedSizeButton(364f, 209f, atlasInterface.findRegion("buttonCloseInventory"));
        atlas = new PrettyRendererAtlas();
        scroll = new NinePatch(atlasInterface.findRegion("scroll"), 7, 7, 7, 7);
        scrollHandle = TextureAtlasHelper.mustFindRegion(atlasInterface, "scrollHandle");

        scores = game.getScoreList();

        scissors = new Rectangle();
        listRect = new Rectangle(PADDING * 2, PADDING + RECORD_SPACING,
                RECORD_WIDTH, (RECORD_HEIGHT + RECORD_SPACING) * 5 - RECORD_SPACING);

        maxScrollY = (RECORD_HEIGHT + RECORD_SPACING) * (scores.size() - 5);

        gestureDetector = new GestureDetector(this);
        tapPoint = new Vector2(0, 0);
        scrollY = -Float.MAX_VALUE;
        pan(0f, 0f, 0f, 0f);

        transformation = new Matrix4();
        this.easing = new EaseOutBack(-MyLittleRogueLike.SCREENHEIGHT, 0f, 0f);
    }

    @Override
    public void render(float delta) {
        timer += delta;
        easing.update(delta);
        titleBackground.update(delta);
        titleBackground.clear();
        final SpriteBatch batch = game.getBatch();

        batch.begin();
        titleBackground.draw(batch);
        batch.end();
        if (isFading() && isFadingComplete()) {
            fadingTo.run();
            return;
        }

        transformation.translate(0, easing.getValue(), 0);
        batch.setTransformMatrix(transformation);
        batch.begin();
        background.draw(batch, PADDING, PADDING,
                MyLittleRogueLike.SCREENWIDTH - PADDING * 2, MyLittleRogueLike.SCREENHEIGHT - PADDING * 2);
        buttonClose.draw(batch);
        scroll.draw(batch, 363f, 13f, 17, 193);
        if (!scores.isEmpty()) {
            batch.draw(scrollHandle, 368f, 17f - (scrollY / maxScrollY) * 177, 8f, 8f);
        }
        batch.end();

        Viewport viewport = game.getViewport();
        viewport.calculateScissors(batch.getTransformMatrix(), listRect, scissors);
        if (ScissorStack.pushScissors(scissors)) {
            batch.begin();
            if (scores.isEmpty()) {
                textFont.setColor(Color.BLACK);
                textFont.draw(batch, "You have not completed any run yet",
                        listRect.x,
                        listRect.y + listRect.height / 2 + textFont.getLineHeight(),
                        listRect.width,
                        Align.center,
                        false
                );
                textFont.setColor(Color.WHITE);
            } else {
                int y = (int) listRect.y - (RECORD_HEIGHT + RECORD_SPACING) + (int) scrollY;
                for (Score score : scores) {
                    TextureAtlas.AtlasRegion portrait = portraits.get(score.sex, score.player);
                    final int x = PADDING + 5 + 5;
                    y += (RECORD_HEIGHT + RECORD_SPACING);
                    listBackground.draw(batch, x, y, RECORD_WIDTH, RECORD_HEIGHT);
                    batch.draw(portrait, x + 5, y + 4);
                    textFont.draw(batch, score.dateTime + "   Play time: " + score.runTimeString, x + 46, y + RECORD_HEIGHT - 4);
                    textFont.draw(batch, "", x + 46, y + 28);
                    if (score.killer != null) {
                        final GlyphLayout layout = textFont.draw(batch, score.playerName + ", killed by", x + 46, y + 20);
                        final CharacterRenderer characterRenderer = atlas.getCharacter(score.killer);
                        characterRenderer.render(batch, x + 46 + (int) layout.width + 4, y + 8, Direction.WEST, timer, 1f);
                        textFont.draw(batch, "at floor " + score.floor, x + 46 + layout.width + 24, y + 20);
                    } else {
                        textFont.draw(batch, score.playerName + ", Game Complete", x + 46, y + 20);
                    }
                }
            }
            batch.end();
            ScissorStack.popScissors();
        }
        transformation.idt();
        batch.setTransformMatrix(transformation);
    }

    private void fadeTo(Runnable runnable) {
        if (isFading()) return;
        easing = new EaseInBack(0f, -MyLittleRogueLike.SCREENHEIGHT, 0f);
        fadingTo = runnable;
    }

    private boolean isFading() {
        return fadingTo != null;
    }

    private boolean isFadingComplete() {
        return easing.isDone();
    }


    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.DOWN:
                return pan(0f, 0f, 0f, 12f);
            case Input.Keys.UP:
                return pan(0f, 0f, 0f, -12f);
            case Input.Keys.ESCAPE:
                returnBack();
                return true;
        }
        return false;
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        return gestureDetector.touchDown(touchPoint.x, touchPoint.y, pointer, button);
    }

    @Override
    public boolean touchUp(Vector2 touchPoint, int pointer, int button) {
        return gestureDetector.touchUp(touchPoint.x, touchPoint.y, pointer, button);
    }

    @Override
    public boolean touchDragged(Vector2 touchPoint, int pointer) {
        return gestureDetector.touchDragged(touchPoint.x, touchPoint.y, pointer);
    }

    @Override
    public void dispose() {
        atlasInterface.dispose();
        atlas.dispose();
        if (titleBackground != null) {
            titleBackground.dispose();
        }
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        tapPoint.set(x, y);
        if (buttonClose.pressed(tapPoint)) {
            returnBack();
            return true;
        }
        return false;
    }

    private void returnBack() {
        fadeTo(new Runnable() {
            @Override
            public void run() {
                game.setScreen(new TitleScreen(game, titleBackground));
                titleBackground = null; // transfer ownership to TitleScreen
                dispose();
            }
        });
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        scrollY = Math.max(Math.min(scrollY + deltaY, 0), -maxScrollY);
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
