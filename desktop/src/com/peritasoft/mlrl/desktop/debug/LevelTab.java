/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.util.adapter.AbstractListAdapter;
import com.kotcrab.vis.ui.widget.ListView;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.item.Item;
import com.peritasoft.mlrl.props.Prop;

public class LevelTab extends Tab {
    private final VisTable content = new VisTable(true);
    private final VisLabel tileLabel = new VisLabel("(none)");
    private final VisLabel propLabel = new VisLabel("(none)");
    private final VisLabel itemLabel = new VisLabel("(none)");
    private final VisLabel demographyLabel = new VisLabel("(none)");
    private final VisLabel levelLabel = new VisLabel("(none)");
    private final VisLabel statsLabel = new VisLabel("(none)");
    private final VisLabel healthPointsLabel = new VisLabel("(none)");
    private final VisLabel magicPointsLabel = new VisLabel("(none)");
    private final VisLabel sightRadiusLabel = new VisLabel("(none)");
    private final VisTextButton killButton = new VisTextButton("Kill");
    private final InventoryAdapter inventoryAdapter = new InventoryAdapter();

    public LevelTab(final Listener listener) {
        super(false, false);

        killButton.setDisabled(true);
        killButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                listener.killCharacter();
            }
        });

        inventoryAdapter.setSelectionMode(AbstractListAdapter.SelectionMode.SINGLE);
        final ListView<Item> inventoryList = new ListView<>(inventoryAdapter);

        content.columnDefaults(0).right();
        content.add(new VisLabel("Tile:"));
        content.add(tileLabel).colspan(2).expandX().fillX();
        content.row();
        content.add(new VisLabel("Prop:"));
        content.add(propLabel).colspan(2).expandX().fillX();
        content.row();
        content.add(new VisLabel("Item:"));
        content.add(itemLabel).colspan(2).expandX().fillX();
        content.row();
        content.addSeparator().colspan(3).row();
        content.add(new VisLabel("Demography:"));
        content.add(demographyLabel).expandX().fillX();
        content.add(killButton).expandX().fillX();
        content.row();
        content.add(new VisLabel("Level:"));
        content.add(levelLabel).colspan(2).expandX().fillX();
        content.row();
        content.add(new VisLabel("Stats:"));
        content.add(statsLabel).colspan(2).expandX().fillX();
        content.row();
        content.add(new VisLabel("HP:"));
        content.add(healthPointsLabel).colspan(2).expandX().fillX();
        content.row();
        content.add(new VisLabel("MP:"));
        content.add(magicPointsLabel).colspan(2).expandX().fillX();
        content.row();
        content.add(new VisLabel("Sight Radius:"));
        content.add(sightRadiusLabel).colspan(2).expandX().fillX();
        content.row();
        content.add(new VisLabel("Inventory:")).top();
        content.add(inventoryList.getMainTable()).colspan(2).expand().fill();

        content.pack();
    }

    @Override
    public String getTabTitle() {
        return "Level";
    }

    @Override
    public Table getContentTable() {
        return content;
    }

    public void setCell(Cell cell) {
        final Prop prop = cell.getProp();
        final Item item = cell.getItem();
        final Character character = cell.getCharacter();
        tileLabel.setText(cell.tile.toString());
        propLabel.setText(prop == null ? "(none)" : prop.getType().toString());
        itemLabel.setText(item == null ? "(none)" : item.getName());
        if (character == null) {
            demographyLabel.setText("(none)");
            levelLabel.setText("(none)");
            statsLabel.setText("(none)");
            healthPointsLabel.setText("(none)");
            magicPointsLabel.setText("(none)");
            sightRadiusLabel.setText("(none)");
            inventoryAdapter.clear();
            killButton.setDisabled(true);
        } else {
            demographyLabel.setText(character.getRace() + " " + character.getKlass());
            levelLabel.setText(character.getLevel());
            statsLabel.setText("STR=" + character.getStr() +
                    " DEX=" + character.getDex() +
                    " WIS=" + character.getWis() +
                    " CON=" + character.getCon());
            healthPointsLabel.setText(character.getHp() + " / " + character.getMaxHp());
            magicPointsLabel.setText(character.getMp() + " / " + character.getMaxMp());
            sightRadiusLabel.setText(character.getSightRadius());
            inventoryAdapter.setInventory(character.getInventory());
            killButton.setDisabled(false);
        }
    }

    public interface Listener {
        void killCharacter();
    }
}
