/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.InventoryRandomizer;
import com.peritasoft.mlrl.item.Item;

import java.util.HashSet;
import java.util.Set;

public class InventoryEditor {
    private Inventory inventory;
    private Demography demography;
    private final Set<Listener> listeners = new HashSet<>();

    public InventoryEditor() {
        this(new Inventory(), Demography.HERO_MAGE);
    }

    public InventoryEditor(Inventory inventory, Demography demography) {
        this.inventory = inventory;
        this.demography = demography;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
        for (Listener listener : listeners) {
            listener.onItemsChanged();
        }
    }

    public void reroll() {
        InventoryRandomizer.randomizeInventory(inventory, demography);
        for (Listener listener : listeners) {
            listener.onItemsChanged();
        }
    }

    public Iterable<Item> iterable() {
        return inventory;
    }

    public int size() {
        return inventory.size();
    }

    public int indexOf(Item item) {
        return inventory.getIndex(item);
    }

    public void add(Item item) {
        inventory.add(item);
        for (Listener listener : listeners) {
            listener.onItemAdded(item);
        }
    }

    public Item get(int index) {
        return inventory.get(index);
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public void setDemography(Demography demography) {
        this.demography = demography;
    }

    public void drop(Iterable<Item> items) {
        for (Item item : items) {
            inventory.remove(item);
        }
        for (Listener listener : listeners) {
            listener.onItemsRemoved(items);
        }
    }

    interface Listener {
        void onItemsChanged();

        void onItemAdded(Item item);

        void onItemsRemoved(Iterable<Item> items);
    }
}
