/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

class RendererTab extends Tab {
    private final Table content = new VisTable(true);

    private static final String FOV_KEY = "field-of-view";
    private static final String CENTER_ON_PLAYER_KEY = "center-on-player";

    public RendererTab(final Preferences preferences, final Listener listener) {
        super(false, false);

        final VisCheckBox centerCheckbox = new VisCheckBox("Center on player");
        centerCheckbox.setChecked(preferences.getBoolean(CENTER_ON_PLAYER_KEY, true));
        ChangeListener centerListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                boolean center = centerCheckbox.isChecked();
                listener.enableCenterOnPlayer(center);
                preferences.putBoolean(CENTER_ON_PLAYER_KEY, center).flush();
            }
        };
        centerCheckbox.addListener(centerListener);
        centerListener.changed(null, null);

        final VisCheckBox fovCheckbox = new VisCheckBox("Field of View");
        fovCheckbox.setChecked(preferences.getBoolean(FOV_KEY, true));
        ChangeListener fovListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                boolean fov = fovCheckbox.isChecked();
                listener.enableFieldOfView(fov);
                centerCheckbox.setDisabled(fov);
                preferences.putBoolean(FOV_KEY, fov).flush();
            }
        };
        fovCheckbox.addListener(fovListener);
        fovListener.changed(null, null);

        final VisTextButton resetPan = new VisTextButton("Reset Pan");
        resetPan.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                listener.resetPan();
            }
        });
        ChangeListener resetPanEnabler = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                resetPan.setDisabled(fovCheckbox.isChecked() || centerCheckbox.isChecked());
            }
        };
        fovCheckbox.addListener(resetPanEnabler);
        centerCheckbox.addListener(resetPanEnabler);
        resetPanEnabler.changed(null, null);

        content.columnDefaults(0).left();
        content.add(fovCheckbox);
        content.row();
        content.add(centerCheckbox);
        content.row();
        content.add(resetPan);
        content.add().expandX().fillX();

        content.pack();
    }

    @Override
    public String getTabTitle() {
        return "Renderer";
    }

    @Override
    public Table getContentTable() {
        return content;
    }

    interface Listener {
        void enableFieldOfView(boolean enable);

        void enableCenterOnPlayer(boolean enable);

        void resetPan();

    }
}
