/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.util.TableUtils;
import com.kotcrab.vis.ui.util.adapter.AbstractListAdapter;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.spinner.FloatSpinnerModel;
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel;
import com.kotcrab.vis.ui.widget.spinner.Spinner;
import com.peritasoft.mlrl.item.*;
import com.peritasoft.mlrl.weapons.*;

import java.math.BigDecimal;

public class InventoryEditWindow extends VisWindow {
    private final InventoryAdapter adapter;

    public InventoryEditWindow() {
        super("Edit Inventory", "resizable");
        setResizable(true);
        setCenterOnAdd(true);
        addCloseButton();
        closeOnEscape();
        TableUtils.setSpacingDefaults(this);

        adapter = new InventoryAdapter();
        adapter.setSelectionMode(AbstractListAdapter.SelectionMode.MULTIPLE);
        final ListView<Item> listView = new ListView<>(adapter);
        final VisTable headerTable = new VisTable();
        headerTable.add("Items").row();
        headerTable.addSeparator();
        listView.setHeader(headerTable);

        final VisTextButton rerollButton = new VisTextButton("Reroll");
        rerollButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                adapter.reroll();
            }
        });

        final VisTextButton dropButton = new VisTextButton("Drop");
        dropButton.setDisabled(true);
        adapter.getSelectionManager().setListener(new AbstractListAdapter.ListSelectionListener<Item, VisTable>() {
            @Override
            public void selected(Item item, VisTable view) {
                dropButton.setDisabled(false);
            }

            @Override
            public void deselected(Item item, VisTable view) {
                dropButton.setDisabled(adapter.getSelection().size == 0);
            }
        });
        dropButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                adapter.dropSelected();
            }
        });

        final VisTable inventoryListTable = new VisTable(true);
        inventoryListTable.add(listView.getMainTable()).colspan(2).grow();
        inventoryListTable.row();
        inventoryListTable.add(dropButton).left();
        inventoryListTable.add(rerollButton).right();

        final VisTable newItemOptions = new VisTable(true);

        final VisSelectBox<ItemOption> itemSelect = new VisSelectBox<>();
        itemSelect.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                ItemOption option = itemSelect.getSelected();
                newItemOptions.clearChildren();
                newItemOptions.add(option.getMainTable()).grow();
            }
        });
        itemSelect.setItems(createItemOptions());

        VisTextButton addButton = new VisTextButton("Add");
        addButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                ItemOption option = itemSelect.getSelected();
                adapter.add(option.createItem());
            }
        });

        VisTable addTableHeader = new VisTable();
        addTableHeader.add("New Item");
        addTableHeader.row();
        addTableHeader.addSeparator();

        final VisTable addTable = new VisTable(true);
        addTable.columnDefaults(0).right();
        addTable.add(addTableHeader).colspan(3).growX();
        addTable.row();
        addTable.add(new VisLabel("Item:"));
        addTable.add(itemSelect).growX();
        addTable.add(addButton);
        addTable.row();
        addTable.addSeparator().colspan(3);
        addTable.add(newItemOptions).colspan(3).grow();

        add(inventoryListTable).growY().padBottom(3);
        add(new Separator()).growY();
        add(addTable).grow().padBottom(3);
        left();

        setSize(600f, 400f);
    }

    private Array<ItemOption> createItemOptions() {
        Array<ItemOption> items = new Array<>();
        items.add(createSwordOptions());
        items.add(createLanceOptions());
        items.add(createDaggerOptions());
        items.add(createBowOptions());
        items.add(createFireSpellgemOptions());
        items.add(createFireGrimoireOptions());
        items.add(createIceGrimoireOptions());
        items.add(createPoisonGrimoireOptions());
        items.add(createLightningGrimoireOptions());
        items.add(createPotionOptions());
        items.add(createScrollOptions());
        return items;
    }

    private ItemOption createSwordOptions() {
        return new MeleeWeaponOption("Sword") {
            @Override
            protected KindOfWeapon generateWeapon(int level) {
                return WeaponGenerator.generateSword(level);
            }

            @Override
            protected KindOfWeapon generateWeapon(int str, int dex, int con, int wis, int damage) {
                return new Sword(str, dex, con, wis, damage);
            }
        };
    }

    private ItemOption createLanceOptions() {
        return new MeleeWeaponOption("Lance") {
            @Override
            protected KindOfWeapon generateWeapon(int level) {
                return WeaponGenerator.generateLance(level);
            }

            @Override
            protected KindOfWeapon generateWeapon(int str, int dex, int con, int wis, int damage) {
                return new Lance(str, dex, con, wis, damage);
            }
        };
    }

    private ItemOption createDaggerOptions() {
        return new MeleeWeaponOption("Dagger") {
            @Override
            protected KindOfWeapon generateWeapon(int level) {
                return WeaponGenerator.generateDagger(level);
            }

            @Override
            protected KindOfWeapon generateWeapon(int str, int dex, int con, int wis, int damage) {
                return new Dagger(str, dex, con, wis, damage);
            }
        };
    }

    private ItemOption createBowOptions() {
        return new ShootableOption("Bow") {
            @Override
            protected KindOfWeapon generateShootable(int level) {
                return WeaponGenerator.generateBow(level);
            }

            @Override
            protected KindOfWeapon generateShootable(int range, int str, int dex, int con, int wis, int damage) {
                return new Bow(range, str, dex, con, wis, damage);
            }
        };
    }

    private ItemOption createFireGrimoireOptions() {
        return new GrimoireOption("Fire Grimoire") {
            @Override
            protected Grimoire generateGrimoire(int level) {
                return WeaponGenerator.generateFireGrimoire(level);
            }

            @Override
            protected Item generateGrimoire(int minWis, int minDistance, int bonusDamage, int manaCost) {
                return new FireGrimoire(minWis, minDistance, bonusDamage, manaCost);
            }
        };
    }

    private ItemOption createIceGrimoireOptions() {
        return new GrimoireOption("Ice Grimoire") {
            @Override
            protected Grimoire generateGrimoire(int level) {
                return WeaponGenerator.generateIceGrimoire(level);
            }

            @Override
            protected Item generateGrimoire(int minWis, int minDistance, int bonusDamage, int manaCost) {
                return new IceGrimoire(minWis, minDistance, bonusDamage, manaCost);
            }
        };
    }

    private ItemOption createPoisonGrimoireOptions() {
        return new GrimoireOption("Poison Grimoire") {
            @Override
            protected Grimoire generateGrimoire(int level) {
                return WeaponGenerator.generatePoisonGrimoire(level);
            }

            @Override
            protected Item generateGrimoire(int minWis, int minDistance, int bonusDamage, int manaCost) {
                return new PoisonGrimoire(minWis, minDistance, bonusDamage, manaCost);
            }
        };
    }

    private ItemOption createLightningGrimoireOptions() {
        return new GrimoireOption("Lightning Grimoire") {
            @Override
            protected Grimoire generateGrimoire(int level) {
                return WeaponGenerator.generateLightningGrimoire(level);
            }

            @Override
            protected Item generateGrimoire(int minWis, int minDistance, int bonusDamage, int manaCost) {
                return new LightningGrimoire(minWis, minDistance, bonusDamage, manaCost);
            }
        };
    }

    public void setEditor(InventoryEditor editor) {
        adapter.setEditor(editor);
    }

    private ItemOption createFireSpellgemOptions() {
        return new ItemOption() {
            private final VisTable mainTable;
            private final IntSpinnerModel uses;
            private final IntSpinnerModel baseDamage;
            private final IntSpinnerModel bonusDamage;
            private final FloatSpinnerModel impactChance;

            {
                uses = new IntSpinnerModel(5, 1, Integer.MAX_VALUE);
                baseDamage = new IntSpinnerModel(4, 4, Integer.MAX_VALUE);
                bonusDamage = new IntSpinnerModel(1, 1, Integer.MAX_VALUE);
                impactChance = new FloatSpinnerModel("0.5", "0.0", "1.0", "0.1");

                final IntSpinnerModel level = new IntSpinnerModel(1, 1, Integer.MAX_VALUE);
                VisTextButton generateButton = new VisTextButton("Generate");
                generateButton.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        FireSpellgem spellgem = WeaponGenerator.generateFireSpellgem(level.getValue());
                        uses.setValue(spellgem.getUses());
                        baseDamage.setValue(spellgem.getBaseDamage());
                        bonusDamage.setValue(spellgem.getBonusDamage());
                        impactChance.setValue(BigDecimal.valueOf(spellgem.getImpactChance()));
                    }
                });

                mainTable = createTableWithLevelOption(level, generateButton);
                mainTable.add("Uses:");
                mainTable.add(new Spinner("", uses));
                mainTable.add("Impact Chance:");
                mainTable.add(new Spinner("", impactChance));
                mainTable.add().growX().row();
                mainTable.add("Base Damage:");
                mainTable.add(new Spinner("", baseDamage));
                mainTable.add("Bonus Damage:");
                mainTable.add(new Spinner("", bonusDamage));
                mainTable.add().growX().row();
                mainTable.add().colspan(5).grow();
            }

            @Override
            public String toString() {
                return "Fire Spellgem";
            }

            @Override
            public Actor getMainTable() {
                return mainTable;
            }

            public Item createItem() {
                return new FireSpellgem(uses.getValue(), baseDamage.getValue(), bonusDamage.getValue(), impactChance.getValue().floatValue());
            }
        };
    }

    private static VisTable createTableWithLevelOption(IntSpinnerModel level, VisTextButton generateButton) {
        final VisTable table = new VisTable(true);
        table.columnDefaults(0).right();
        table.columnDefaults(2).right();
        table.add("Level:");
        table.add(new Spinner("", level));
        table.add(generateButton).colspan(2).fill();
        table.add().growX().row();
        table.addSeparator().colspan(5);
        return table;
    }

    private ItemOption createPotionOptions() {
        return new ItemOption() {
            private final VisTable mainTable;
            private final VisSelectBox<PotionColour> colour;

            {
                colour = new VisSelectBox<>();
                colour.setItems(PotionColour.values());

                mainTable = new VisTable(true);
                mainTable.add("Colour:");
                mainTable.add(colour);
                mainTable.add().growX().row();
                mainTable.add().colspan(5).grow();
            }

            @Override
            public String toString() {
                return "Potion";
            }

            @Override
            public Actor getMainTable() {
                return mainTable;
            }

            public Item createItem() {
                return new Potion(colour.getSelected());
            }
        };
    }

    private ItemOption createScrollOptions() {
        return new ItemOption() {
            private final VisTable mainTable;
            private final IntSpinnerModel level;
            private final VisSelectBox<String> type;

            {
                level = new IntSpinnerModel(1, 1, Integer.MAX_VALUE);

                type = new VisSelectBox<>();
                type.setItems("Fire", "Ice", "Petrification", "Poision", "Teleport");

                mainTable = new VisTable(true);
                mainTable.columnDefaults(0).right();
                mainTable.columnDefaults(1).fill();
                mainTable.add("Level:");
                mainTable.add(new Spinner("", level));
                mainTable.add().growX().row();
                mainTable.add("Type:");
                mainTable.add(type);
                mainTable.add().growX().row();
                mainTable.add().colspan(5).grow();
            }

            @Override
            public String toString() {
                return "Scroll";
            }

            @Override
            public Actor getMainTable() {
                return mainTable;
            }

            public Item createItem() {
                return ItemGenerator.generateScroll(type.getSelectedIndex(), level.getValue());
            }
        };
    }

    private interface ItemOption {
        Actor getMainTable();

        Item createItem();
    }

    private static abstract class MeleeWeaponOption implements ItemOption {
        private final String label;
        private final VisTable mainTable;
        private final IntSpinnerModel str;
        private final IntSpinnerModel dex;
        private final IntSpinnerModel con;
        private final IntSpinnerModel wis;
        private final IntSpinnerModel damage;

        public MeleeWeaponOption(String label) {
            this.label = label;
            str = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            dex = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            con = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            wis = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            damage = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);

            final IntSpinnerModel level = new IntSpinnerModel(2, 2, Integer.MAX_VALUE);
            VisTextButton generateButton = new VisTextButton("Generate");
            generateButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    KindOfWeapon weapon = generateWeapon(level.getValue());
                    str.setValue(weapon.getBonusStr());
                    dex.setValue(weapon.getBonusDex());
                    con.setValue(weapon.getBonusCon());
                    wis.setValue(weapon.getBonusWis());
                    damage.setValue(weapon.getBonusDamage());
                }
            });

            mainTable = createTableWithLevelOption(level, generateButton);
            mainTable.add("Str:");
            mainTable.add(new Spinner("", str));
            mainTable.add("Dex:");
            mainTable.add(new Spinner("", dex));
            mainTable.add().growX().row();
            mainTable.add("Con:");
            mainTable.add(new Spinner("", con));
            mainTable.add("Wis:");
            mainTable.add(new Spinner("", wis));
            mainTable.add().growX().row();
            mainTable.add("Damage:");
            mainTable.add(new Spinner("", damage));
            mainTable.row();
            mainTable.add().colspan(5).grow();
        }

        @Override
        public String toString() {
            return label;
        }

        @Override
        public Actor getMainTable() {
            return mainTable;
        }

        public Item createItem() {
            return generateWeapon(str.getValue(), dex.getValue(), con.getValue(), wis.getValue(), damage.getValue());
        }

        abstract protected KindOfWeapon generateWeapon(int level);

        abstract protected KindOfWeapon generateWeapon(int str, int dex, int con, int wis, int damage);
    }

    private static abstract class ShootableOption implements ItemOption {
        private final String label;
        private final VisTable mainTable;
        private final IntSpinnerModel str;
        private final IntSpinnerModel dex;
        private final IntSpinnerModel con;
        private final IntSpinnerModel wis;
        private final IntSpinnerModel damage;
        private final IntSpinnerModel range;

        public ShootableOption(String label) {
            this.label = label;
            str = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            dex = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            con = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            wis = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            damage = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            range = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);

            final IntSpinnerModel level = new IntSpinnerModel(2, 2, Integer.MAX_VALUE);
            VisTextButton generateButton = new VisTextButton("Generate");
            generateButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    KindOfWeapon weapon = generateShootable(level.getValue());
                    str.setValue(weapon.getBonusStr());
                    dex.setValue(weapon.getBonusDex());
                    con.setValue(weapon.getBonusCon());
                    wis.setValue(weapon.getBonusWis());
                    damage.setValue(weapon.getBonusDamage());
                    range.setValue(((Shootable) weapon).getRange());
                }
            });

            mainTable = createTableWithLevelOption(level, generateButton);
            mainTable.add("Range:");
            mainTable.add(new Spinner("", range));
            mainTable.add("Str:");
            mainTable.add(new Spinner("", str));
            mainTable.add().growX().row();
            mainTable.add("Dex:");
            mainTable.add(new Spinner("", dex));
            mainTable.add("Con:");
            mainTable.add(new Spinner("", con));
            mainTable.add().growX().row();
            mainTable.add("Wis:");
            mainTable.add(new Spinner("", wis));
            mainTable.add("Damage:");
            mainTable.add(new Spinner("", damage));
            mainTable.row();
            mainTable.add().colspan(5).grow();
        }

        @Override
        public String toString() {
            return label;
        }

        @Override
        public Actor getMainTable() {
            return mainTable;
        }

        public Item createItem() {
            return generateShootable(range.getValue(), str.getValue(), dex.getValue(), con.getValue(), wis.getValue(), damage.getValue());
        }

        abstract protected KindOfWeapon generateShootable(int level);

        abstract protected KindOfWeapon generateShootable(int range, int str, int dex, int con, int wis, int damage);
    }

    private static abstract class GrimoireOption implements ItemOption {
        private final String label;
        private final VisTable mainTable;
        private final IntSpinnerModel minWis;
        private final IntSpinnerModel minDistance;
        private final IntSpinnerModel damage;
        private final IntSpinnerModel manaCost;

        public GrimoireOption(String label) {
            this.label = label;
            minWis = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            minDistance = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            damage = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);
            manaCost = new IntSpinnerModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE);

            final IntSpinnerModel level = new IntSpinnerModel(2, 2, Integer.MAX_VALUE);
            VisTextButton generateButton = new VisTextButton("Generate");
            generateButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    Grimoire grimoire = generateGrimoire(level.getValue());
                    minWis.setValue(grimoire.getMinWis());
                    minDistance.setValue(grimoire.getMinDistance());
                    damage.setValue(grimoire.getBonusDamage());
                    manaCost.setValue(grimoire.getManaCost());
                }
            });

            mainTable = createTableWithLevelOption(level, generateButton);
            mainTable.add("Min Wis:");
            mainTable.add(new Spinner("", minWis));
            mainTable.add("Min Distance:");
            mainTable.add(new Spinner("", minDistance));
            mainTable.add().growX().row();
            mainTable.add("Damage:");
            mainTable.add(new Spinner("", damage));
            mainTable.add("Mana Cost:");
            mainTable.add(new Spinner("", manaCost));
            mainTable.add().growX().row();
            mainTable.add().colspan(5).grow();
        }

        @Override
        public String toString() {
            return label;
        }

        @Override
        public Actor getMainTable() {
            return mainTable;
        }

        public Item createItem() {
            return generateGrimoire(minWis.getValue(), minDistance.getValue(), damage.getValue(), manaCost.getValue());
        }

        abstract protected Grimoire generateGrimoire(int level);

        abstract protected Item generateGrimoire(int minWis, int minDistance, int bonusDamage, int manaCost);
    }
}
