/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.desktop.debug;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.util.form.SimpleFormValidator;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel;
import com.kotcrab.vis.ui.widget.spinner.Spinner;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.characters.Sex;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;

import java.util.Random;

class GeneratorTab extends Tab {
    public static final String FLOOR_WIDTH_KEY = "floor.width";
    public static final String FLOOR_HEIGHT_KEY = "floor.height";
    public static final String FLOOR_NUMBER_KEY = "floor.number";
    public static final String HERO_STR_KEY = "hero.str";
    public static final String HERO_DEX_KEY = "hero.dex";
    public static final String HERO_WIS_KEY = "hero.wis";
    public static final String HERO_CON_KEY = "hero.con";
    public static final String HERO_LEVEL_KEY = "hero.level";
    public static final String HERO_DEMOGRAPHY_INDEX_KEY = "hero.demography-index";
    public static final String INVENTORY_STRING = "inventory";

    private final Table content = new VisTable(true);
    private final Json json;

    public GeneratorTab(final Json json, final Preferences preferences, final Listener listener) {
        super(false, false);
        content.columnDefaults(0).right();
        content.columnDefaults(2).right();

        this.json = json;

        VisTextButton generateButton = new VisTextButton("Generate");
        VisTextButton randomButton = new VisTextButton("Random Seed");

        final VisValidatableTextField seedField = new VisValidatableTextField(String.valueOf(MathUtils.random(Long.MAX_VALUE)));
        final IntSpinnerModel widthModel = new IntSpinnerModel(preferences.getInteger(FLOOR_WIDTH_KEY, 30), 20, 500, 1);
        final IntSpinnerModel heightModel = new IntSpinnerModel(preferences.getInteger(FLOOR_HEIGHT_KEY, 30), 20, 500, 1);
        final IntSpinnerModel floorModel = new IntSpinnerModel(preferences.getInteger(FLOOR_NUMBER_KEY, 1), 1, 500, 1);
        final IntSpinnerModel strModel = new IntSpinnerModel(preferences.getInteger(HERO_STR_KEY, -1), -1, 500);
        final IntSpinnerModel dexModel = new IntSpinnerModel(preferences.getInteger(HERO_DEX_KEY, -1), -1, 500);
        final IntSpinnerModel wisModel = new IntSpinnerModel(preferences.getInteger(HERO_WIS_KEY, -1), -1, 500);
        final IntSpinnerModel conModel = new IntSpinnerModel(preferences.getInteger(HERO_CON_KEY, -1), -1, 500);
        final IntSpinnerModel levelModel = new IntSpinnerModel(preferences.getInteger(HERO_LEVEL_KEY, 1), 1, Integer.MAX_VALUE);

        final VisSelectBox<Demography> demographySelect = new VisSelectBox<>();
        demographySelect.setItems(
                Demography.HERO_WARRIOR,
                Demography.HERO_ARCHER,
                Demography.HERO_MAGE
        );
        demographySelect.setSelectedIndex(preferences.getInteger(HERO_DEMOGRAPHY_INDEX_KEY, 0));

        final Inventory inventory = readInventoryOrEmpty(preferences);
        final InventoryEditor inventoryEditor = new InventoryEditor(inventory, demographySelect.getSelected());
        final InventoryAdapter inventoryAdapter = new InventoryAdapter(inventoryEditor);
        final ListView<Item> inventoryList = new ListView<>(inventoryAdapter);
        if (inventoryEditor.size() == 0) {
            inventoryEditor.reroll();
        }
        demographySelect.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                inventoryEditor.setDemography(demographySelect.getSelected());
            }
        });

        final LinkLabel inventoryLink = new LinkLabel("Inventory:");
        inventoryLink.setListener(new LinkLabel.LinkLabelListener() {
            @Override
            public void clicked(String url) {
                listener.openInventoryEditWindow(inventoryEditor);
            }
        });

        VisLabel errorLabel = new VisLabel();
        errorLabel.setColor(Color.RED);

        VisTable buttonTable = new VisTable(true);
        buttonTable.add(errorLabel).expand().fill();
        buttonTable.add(generateButton);

        content.add(new VisLabel("Seed:"));
        content.add(seedField).expand().fill().colspan(2);
        content.add(randomButton).expand().fill();
        content.row();
        content.add(new VisLabel("Width:"));
        content.add(new Spinner("", widthModel)).expand().fill();
        content.add(new VisLabel("Height:"));
        content.add(new Spinner("", heightModel)).expand().fill();
        content.row();
        content.add(new VisLabel("Floor:"));
        content.add(new Spinner("", floorModel)).expand().fill().colspan(3);
        content.row();
        content.add(new VisLabel("Class:"));
        content.add(demographySelect).expand().fill();
        content.add(new VisLabel("Level:"));
        content.add(new Spinner("", levelModel)).expand().fill();
        content.row();
        content.add(new VisLabel("Str:"));
        content.add(new Spinner("", strModel)).expand().fill();
        content.add(new VisLabel("Dex:"));
        content.add(new Spinner("", dexModel)).expand().fill();
        content.row();
        content.add(new VisLabel("Wis:"));
        content.add(new Spinner("", wisModel)).expand().fill();
        content.add(new VisLabel("Con:"));
        content.add(new Spinner("", conModel)).expand().fill();
        content.row();
        content.add(inventoryLink).top();
        content.add(inventoryList.getMainTable()).expandX().fillX().colspan(3);
        content.row();
        content.add(buttonTable).fill().expand().colspan(4).padBottom(3);

        SimpleFormValidator validator = new SimpleFormValidator(generateButton, errorLabel, "smooth");
        validator.notEmpty(seedField, "Seed can not be empty");

        randomButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                seedField.setText(String.valueOf(new Random().nextLong()));
            }
        });

        generateButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                long seed = Long.parseLong(seedField.getText());
                int floor = floorModel.getValue();
                int width = widthModel.getValue();
                int height = heightModel.getValue();
                int heroLevel = levelModel.getValue();
                Demography demography = demographySelect.getSelected();
                int str = getValue(strModel, PlayerHero.initialStr(demography));
                int dex = getValue(dexModel, PlayerHero.initialDex(demography));
                int wis = getValue(wisModel, PlayerHero.initialWis(demography));
                int con = getValue(conModel, PlayerHero.initialCon(demography));

                preferences
                        .putInteger(FLOOR_WIDTH_KEY, width)
                        .putInteger(FLOOR_HEIGHT_KEY, height)
                        .putInteger(FLOOR_NUMBER_KEY, floor)
                        .putInteger(HERO_STR_KEY, strModel.getValue())
                        .putInteger(HERO_DEX_KEY, dexModel.getValue())
                        .putInteger(HERO_WIS_KEY, wisModel.getValue())
                        .putInteger(HERO_CON_KEY, conModel.getValue())
                        .putInteger(HERO_LEVEL_KEY, heroLevel)
                        .putInteger(HERO_DEMOGRAPHY_INDEX_KEY, demographySelect.getSelectedIndex())
                        .putString(INVENTORY_STRING, json.toJson(inventory))
                        .flush()
                ;

                final Inventory playerInventory = json.fromJson(Inventory.class, json.toJson(inventory));
                PlayerHero hero = new PlayerHero(demography, Sex.FEMALE, 1, str, dex, wis, con, playerInventory);
                hero.equip(playerInventory.get(0));
                listener.generateLevel(seed, floor, width, height, hero, heroLevel);
            }
        });

        content.pack();
    }

    private Inventory readInventoryOrEmpty(Preferences preferences) {
        final String serializedInventory = preferences.getString(INVENTORY_STRING, "");
        if (serializedInventory == null || serializedInventory.isEmpty()) {
            return new Inventory();
        }
        try {
            return json.fromJson(Inventory.class, serializedInventory);
        } catch (Throwable e) {
            System.err.println(e.toString());
            return new Inventory();
        }
    }

    private static int getValue(IntSpinnerModel model, int defaultValue) {
        return model.getValue() < 0 ? defaultValue : model.getValue();
    }

    @Override
    public String getTabTitle() {
        return "Generator";
    }

    @Override
    public Table getContentTable() {
        return content;
    }

    public interface Listener {
        void generateLevel(long seed, int floor, int width, int height, final PlayerHero hero, int heroLevel);

        void openInventoryEditWindow(InventoryEditor editor);
    }
}
